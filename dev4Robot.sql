/*
 Navicat Premium Data Transfer

 Source Server         : PostGres
 Source Server Type    : PostgreSQL
 Source Server Version : 90112
 Source Host           : anonyme77.no-ip.org
 Source Database       : dev4Robot
 Source Schema         : dev4Robot

 Target Server Type    : PostgreSQL
 Target Server Version : 90112
 File Encoding         : utf-8

 Date: 04/23/2014 21:52:11 PM
*/

-- ----------------------------
--  Table structure for robot
-- ----------------------------
DROP TABLE IF EXISTS "dev4Robot"."robot";
CREATE TABLE "dev4Robot"."robot" (
	"codesource" varchar(30) NOT NULL COLLATE "default",
	"nom" varchar(30) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "dev4Robot"."robot" OWNER TO "dev4Robot";

-- ----------------------------
--  Table structure for utilisateur
-- ----------------------------
DROP TABLE IF EXISTS "dev4Robot"."utilisateur";
CREATE TABLE "dev4Robot"."utilisateur" (
	"pseudonyme" varchar(30) NOT NULL COLLATE "default",
	"email" varchar(50) NOT NULL COLLATE "default",
	"password" varchar(30) NOT NULL COLLATE "default",
	"image" char(60) COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "dev4Robot"."utilisateur" OWNER TO "dev4Robot";

-- ----------------------------
--  Records of utilisateur
-- ----------------------------
BEGIN;
INSERT INTO "dev4Robot"."utilisateur" VALUES ('iSanzo', 'isanzo@me.com', 'jesuisSanzo', '                                                            ');
INSERT INTO "dev4Robot"."utilisateur" VALUES ('Anonyme77', 'laurent@gmail.com', 'jesuisLaurent', '                                                            ');
INSERT INTO "dev4Robot"."utilisateur" VALUES ('Loup02', 'loup02@gmail.com', 'jesuisJerome', '                                                            ');
INSERT INTO "dev4Robot"."utilisateur" VALUES ('Bubble', 'anthony@gmail.com', 'jesuisAnthony', '                                                            ');
INSERT INTO "dev4Robot"."utilisateur" VALUES ('Lol', 'bleu', 'bleu', null);
COMMIT;

-- ----------------------------
--  Table structure for bataille
-- ----------------------------
DROP TABLE IF EXISTS "dev4Robot"."bataille";
CREATE TABLE "dev4Robot"."bataille" (
	"nom" varchar(30) NOT NULL COLLATE "default",
	"email" varchar(50) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "dev4Robot"."bataille" OWNER TO "dev4Robot";

-- ----------------------------
--  Table structure for appartenance
-- ----------------------------
DROP TABLE IF EXISTS "dev4Robot"."appartenance";
CREATE TABLE "dev4Robot"."appartenance" (
	"id" numeric(10,0) NOT NULL,
	"nom" varchar(30) NOT NULL COLLATE "default",
	"email" varchar(50) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "dev4Robot"."appartenance" OWNER TO "dev4Robot";

-- ----------------------------
--  Table structure for combattant
-- ----------------------------
DROP TABLE IF EXISTS "dev4Robot"."combattant";
CREATE TABLE "dev4Robot"."combattant" (
	"id" numeric(10,0) NOT NULL,
	"nom" varchar(30) NOT NULL COLLATE "default",
	"ins_nom" varchar(30) NOT NULL COLLATE "default"
)
WITH (OIDS=FALSE);
ALTER TABLE "dev4Robot"."combattant" OWNER TO "dev4Robot";

-- ----------------------------
--  Primary key structure for table robot
-- ----------------------------
ALTER TABLE "dev4Robot"."robot" ADD CONSTRAINT "idrobots" PRIMARY KEY ("nom") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Triggers structure for table robot
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16611" AFTER UPDATE ON "dev4Robot"."robot" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16611" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16610" AFTER DELETE ON "dev4Robot"."robot" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16610" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16522" AFTER DELETE ON "dev4Robot"."robot" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16522" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16523" AFTER UPDATE ON "dev4Robot"."robot" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16523" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16626" AFTER UPDATE ON "dev4Robot"."robot" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16626" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16625" AFTER DELETE ON "dev4Robot"."robot" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16625" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16517" AFTER DELETE ON "dev4Robot"."robot" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16517" ON "dev4Robot"."robot" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16518" AFTER UPDATE ON "dev4Robot"."robot" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16518" ON "dev4Robot"."robot" IS NULL;

-- ----------------------------
--  Primary key structure for table utilisateur
-- ----------------------------
ALTER TABLE "dev4Robot"."utilisateur" ADD CONSTRAINT "idutilisateurs" PRIMARY KEY ("email") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Triggers structure for table utilisateur
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16631" AFTER UPDATE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16631" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16630" AFTER DELETE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16630" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16507" AFTER DELETE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16507" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16508" AFTER UPDATE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16508" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16616" AFTER UPDATE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16616" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16615" AFTER DELETE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16615" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16527" AFTER DELETE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16527" ON "dev4Robot"."utilisateur" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16528" AFTER UPDATE ON "dev4Robot"."utilisateur" FROM "dev4Robot"."appartenance" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16528" ON "dev4Robot"."utilisateur" IS NULL;

-- ----------------------------
--  Primary key structure for table bataille
-- ----------------------------
ALTER TABLE "dev4Robot"."bataille" ADD CONSTRAINT "idbataille_id" PRIMARY KEY ("nom") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table bataille
-- ----------------------------
CREATE INDEX  "fkcreation_ind" ON "dev4Robot"."bataille" USING btree(email COLLATE "default" ASC NULLS LAST);

-- ----------------------------
--  Triggers structure for table bataille
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16633" AFTER UPDATE ON "dev4Robot"."bataille" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16633" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16632" AFTER INSERT ON "dev4Robot"."bataille" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16632" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16509" AFTER INSERT ON "dev4Robot"."bataille" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16509" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16510" AFTER UPDATE ON "dev4Robot"."bataille" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16510" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16621" AFTER UPDATE ON "dev4Robot"."bataille" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16621" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16620" AFTER DELETE ON "dev4Robot"."bataille" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16620" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16512" AFTER DELETE ON "dev4Robot"."bataille" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_del"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16512" ON "dev4Robot"."bataille" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16513" AFTER UPDATE ON "dev4Robot"."bataille" FROM "dev4Robot"."combattant" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_noaction_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16513" ON "dev4Robot"."bataille" IS NULL;

-- ----------------------------
--  Primary key structure for table appartenance
-- ----------------------------
ALTER TABLE "dev4Robot"."appartenance" ADD CONSTRAINT "idappartenance" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table appartenance
-- ----------------------------
CREATE INDEX  "fka_ind" ON "dev4Robot"."appartenance" USING btree(email COLLATE "default" ASC NULLS LAST);

-- ----------------------------
--  Triggers structure for table appartenance
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16618" AFTER UPDATE ON "dev4Robot"."appartenance" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16618" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16617" AFTER INSERT ON "dev4Robot"."appartenance" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16617" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16529" AFTER INSERT ON "dev4Robot"."appartenance" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16529" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16530" AFTER UPDATE ON "dev4Robot"."appartenance" FROM "dev4Robot"."utilisateur" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16530" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16613" AFTER UPDATE ON "dev4Robot"."appartenance" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16613" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16612" AFTER INSERT ON "dev4Robot"."appartenance" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16612" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16524" AFTER INSERT ON "dev4Robot"."appartenance" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16524" ON "dev4Robot"."appartenance" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16525" AFTER UPDATE ON "dev4Robot"."appartenance" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16525" ON "dev4Robot"."appartenance" IS NULL;

-- ----------------------------
--  Primary key structure for table combattant
-- ----------------------------
ALTER TABLE "dev4Robot"."combattant" ADD CONSTRAINT "idcombattant" PRIMARY KEY ("id") NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Indexes structure for table combattant
-- ----------------------------
CREATE INDEX  "fkinscription_ind" ON "dev4Robot"."combattant" USING btree(ins_nom COLLATE "default" ASC NULLS LAST);

-- ----------------------------
--  Triggers structure for table combattant
-- ----------------------------
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16623" AFTER UPDATE ON "dev4Robot"."combattant" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16623" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16622" AFTER INSERT ON "dev4Robot"."combattant" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16622" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16514" AFTER INSERT ON "dev4Robot"."combattant" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16514" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16515" AFTER UPDATE ON "dev4Robot"."combattant" FROM "dev4Robot"."bataille" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16515" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16628" AFTER UPDATE ON "dev4Robot"."combattant" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16628" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16627" AFTER INSERT ON "dev4Robot"."combattant" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16627" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16519" AFTER INSERT ON "dev4Robot"."combattant" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_ins"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16519" ON "dev4Robot"."combattant" IS NULL;
CREATE CONSTRAINT TRIGGER "RI_ConstraintTrigger_16520" AFTER UPDATE ON "dev4Robot"."combattant" FROM "dev4Robot"."robot" NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE PROCEDURE "RI_FKey_check_upd"();
COMMENT ON TRIGGER "RI_ConstraintTrigger_16520" ON "dev4Robot"."combattant" IS NULL;

-- ----------------------------
--  Foreign keys structure for table bataille
-- ----------------------------
ALTER TABLE "dev4Robot"."bataille" ADD CONSTRAINT "fkcreation_fk" FOREIGN KEY ("email") REFERENCES "dev4Robot"."utilisateur" ("email") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table appartenance
-- ----------------------------
ALTER TABLE "dev4Robot"."appartenance" ADD CONSTRAINT "fka_fk" FOREIGN KEY ("email") REFERENCES "dev4Robot"."utilisateur" ("email") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "dev4Robot"."appartenance" ADD CONSTRAINT "fkliea" FOREIGN KEY ("nom") REFERENCES "dev4Robot"."robot" ("nom") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

-- ----------------------------
--  Foreign keys structure for table combattant
-- ----------------------------
ALTER TABLE "dev4Robot"."combattant" ADD CONSTRAINT "fkcomposition" FOREIGN KEY ("nom") REFERENCES "dev4Robot"."bataille" ("nom") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE "dev4Robot"."combattant" ADD CONSTRAINT "fkinscription_fk" FOREIGN KEY ("ins_nom") REFERENCES "dev4Robot"."robot" ("nom") ON UPDATE NO ACTION ON DELETE NO ACTION NOT DEFERRABLE INITIALLY IMMEDIATE;

