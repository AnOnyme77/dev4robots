package be.iesca.domaine;

import java.util.ArrayList;

import be.iesca.util.Author;
import be.iesca.util.Version;

@Author(auteur="Jamar")
@Version(version = 1)
public class Bataille {
	private String nom;
	private String email;
	private ArrayList<String> listeRobots;
	
	public Bataille(){
		super();
		this.nom = "";
		this.email = "";
		this.listeRobots = new ArrayList<String>();
	}
	
	public Bataille(Bataille bataille){
		this.nom = new String(bataille.getNom());
		this.email = new String(bataille.getEmail());
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return nom;
	}

	public boolean ajouterRobot(String robot){
		boolean retour=false;
		if(!this.listeRobots.contains(robot))
			retour=this.listeRobots.add(robot);
		return retour;
	}
	
	public boolean supprimerRobot(String robot){
		boolean retour=false;
		if(this.listeRobots.contains(robot))
			retour=this.listeRobots.remove(robot);
			
		return retour;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bataille other = (Bataille) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	public String[] listeRobots(){
		String[] retour = new String[this.listeRobots.size()];
		int i=0;
		for(String chaine:this.listeRobots){
			retour[i]=chaine;
			i++;
		}
		return(retour);
	}
	
}
