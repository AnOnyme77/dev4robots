package be.iesca.domaine;

import java.io.File;
import java.io.IOException;

import robocode.Robot;
import be.iesca.util.Author;
import be.iesca.util.Compilateur;
import be.iesca.util.Fichier;
import be.iesca.util.Version;

public class ClasseRobot {
	public static final String PACKAGE_ROBOTS = "robots";
	private String nom; // nom simple de la classe. Ex: "Fire" (de
						// "robots.Fire.java")
	public ClasseRobot() {
		super();
	}
	
	@Author(auteur="Evrard")
	@Version(version = 1)
	public ClasseRobot(String nom, String path) throws Exception {
		super();
		this.nom = nom;

		// vérification de la présence du fichier
		File file = new File(path,nom+".java");
		
		if (file == null || !file.exists())
			throw new IOException(
					"Fichier inexistant ou null");
		
		File lib = new File("libs","robocode.jar");
		if (lib == null || !lib.exists())
			throw new IOException(
					"Librairie inexistante ou null");
		
		// vérification compilation
		String[] librairies = { "./libs/robocode.jar" };

		boolean sortieConsole = true;
		
		Compilateur compilateur = new Compilateur(path, librairies,
				"./bin", sortieConsole);
		compilateur.compiler();
		if(!compilateur.compilationReussie()){
			throw new Exception("Classe non compilable");
		}
		
		//Copie du fichier
//		System.out.println("Copie du fichier");
		try{
			Fichier.copierFichier(nom+".java", path, Bundle.REPERTOIRE_ROBOTS);
		}
		catch(Exception e){
			System.out.println("Le fichier existe déjà");
		}
		
		// Chargement de la classe
		// à placer après la compilation (sinon ClassNotFoundException)
		Class<?> cl = Class.forName("robots."+nom);
		
		// Si la classe n'hérite pas de Robot, il doit lancer une ClassCastException
		cl.asSubclass(Robot.class);
	}

	public ClasseRobot(ClasseRobot robot) {
		super();
		this.nom = robot.nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return nom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClasseRobot other = (ClasseRobot) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
}
