package be.iesca.domaine;


public class User {
	private String pseudo;
	private String email; // identifiant unique
	private String password;
	private String image;

	public User() {
		super();
		this.email = "";
		this.pseudo = "";
		this.password = "";
		this.image = null;
	}

	public User(User user) {
		this.email = new String(user.getEmail());
		this.pseudo = new String(user.getPseudo());
		this.password = new String(user.getPassword());
		this.image = user.getImage();
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
		this.pseudo = "";
		this.image = null;
	}

	public User(String email, String pseudo, String password, String image) {
		this.email = email;
		this.password = password;
		this.pseudo = pseudo;
		this.image = image;
	}

	public User(String email, String pseudo, String password) {
		this.email = email;
		this.password = password;
		this.pseudo = pseudo;
		this.image = null;
	}

	@Override
	public String toString() {
		return "User [email=" + email + ", nom=" + pseudo + ", motDePasse="
				+ password + "]";
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

}
