package be.iesca.domaine;

import be.iesca.ihm.LancementBataille;

public class ThreadBataille extends Thread {
	
	private String sourcePath;
	private String [] listeRobots;
	
	public ThreadBataille(String source,String []robots){
		this.sourcePath=source;
		this.listeRobots=robots;
	}
	
	public void run(){
		try{
			LancementBataille lb = new LancementBataille(this.sourcePath, this.listeRobots);
			lb.demarrer();
		}
		catch(Exception e){
			System.out.println("Arrêt du thread");
		}
	}
}
