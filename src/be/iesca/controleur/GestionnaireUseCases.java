package be.iesca.controleur;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;
import be.iesca.usecaseimpl.GestionBataillesImpl;
import be.iesca.usecaseimpl.GestionRobotsImpl;
import be.iesca.usecaseimpl.GestionUsersImpl;
import be.iesca.util.Author;
import be.iesca.util.Version;
/**
 * Controleur de l'application (couche logique)
 * C'est un singleton.
 * @author Olivier Legrand
 *
 */

@Author( auteur="Legrand", reviseur={"Lammens"})
@Version(version = 1)
public class GestionnaireUseCases implements GestionUsers {

	private static final GestionnaireUseCases INSTANCE = new GestionnaireUseCases();
	
	private User user; // null si pas identifie par le systeme
	private GestionUsersImpl gestionUsers;
	private GestionRobotsImpl gestionRobots;
	private GestionBataillesImpl gestionBataille;

	public static GestionnaireUseCases getInstance() {
		return INSTANCE;
	}

	private GestionnaireUseCases() {
		this.gestionUsers = new GestionUsersImpl();
		this.gestionRobots =  new GestionRobotsImpl();
		this.gestionBataille =  new GestionBataillesImpl();
		this.user = null; // pas de user connecte
	}

	@Author( auteur="Legrand", reviseur={"Lammens"})
	@Version(version = 1)
	@Override
	public void connecterUser(Bundle bundle) {
		if (user == null) {
			this.gestionUsers.connecterUser(bundle);;
			if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) {
				this.user = (User) bundle.get(Bundle.USER);
			}
		} else { // un utilisateur est deja connecte
			bundle.put(Bundle.MESSAGE,
					"Opération impossible. Un utilisateur est déjà connecté.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		}
	}

	@Author( auteur="Legrand", reviseur={"Lammens"})
	@Version(version = 1)
	public void deconnecterUser(Bundle bundle) {
		if (user == null) { // pas de user identifie
			bundle.put(Bundle.MESSAGE,
					"Opération impossible. Pas d'utilisateur connecté.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		} else {
			this.user = null; // utilisateur deconnecte
			bundle.put(Bundle.MESSAGE, "À bientôt !");
			bundle.put(Bundle.OPERATION_REUSSIE, true);
		}
	}

	@Author( auteur="Lammens")
	@Version(version = 2)
	@Override
	public void ajouterUser(Bundle bundle) {
			this.gestionUsers.ajouterUser(bundle);
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public void modifierUser(Bundle bundle) {
		if (user == null) { // pas de user identifie
			bundle.put(Bundle.MESSAGE,
					"Opération impossible. Pas d'utilisateur connecté.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		} else {
			this.gestionUsers.modifierUser(bundle);
		}
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	public void afficherUser(Bundle bundle) {
		if (user == null) { // pas de user identifie
			bundle.put(Bundle.MESSAGE,
					"Opération impossible. Pas d'utilisateur connecté.");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		} else {
			bundle.put(Bundle.USER, this.user);
			bundle.put(Bundle.MESSAGE, "Profil de l'utlisateur : "+this.user.getPseudo());
			bundle.put(Bundle.OPERATION_REUSSIE, true);
		}
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void ajouterRobot(Bundle bundle){
		this.gestionRobots.ajouterRobot(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void supprimerRobot(Bundle bundle){
		this.gestionRobots.supprimerRobot(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void listerRobots(Bundle bundle){
		this.gestionRobots.listerRobots(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void listerRobotUsers(Bundle bundle){
		this.gestionRobots.listerRobotsUser(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void ajouterBataille(Bundle bundle){
		this.gestionBataille.ajouterBataille(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void listerBataille(Bundle bundle){
		this.gestionBataille.listerBataille(bundle);
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	public void recupererBataille(Bundle bundle){
		this.gestionBataille.recupererBataille(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void modifierBataille(Bundle bundle){
		this.gestionBataille.modifierBataille(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void lancerBataille(Bundle bundle){
		this.gestionBataille.lancerBataille(bundle);
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public void supprimerBataille(Bundle bundle){
		this.gestionBataille.supprimerBataille(bundle);
	}
	
	public User getUser(){
		return this.user;
	}
}
