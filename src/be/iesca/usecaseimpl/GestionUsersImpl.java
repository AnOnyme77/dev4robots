package be.iesca.usecaseimpl;

import java.util.regex.Pattern;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author( auteur="Legrand", reviseur={"Hainaut"})
@Version(version = 1)
public class GestionUsersImpl implements GestionUsers {
	
	private static final int MDP = 6;
	private UserDao userDao;

	public GestionUsersImpl() {
		this.userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(
				UserDao.class);
	}

	@Author( auteur="Legrand", reviseur={"Hainaut"})
	@Version
	@Override
	public void connecterUser(Bundle bundle) {
		User userDB = null;
		boolean operationReussie = false;
		String message;
		User user = (User) bundle.get(Bundle.USER);
		if (user == null) {
			message = "Pas d'utilisateur!";
		} else {
			String email = user.getEmail();
			String password = user.getPassword();
			userDB = this.userDao.getUser(email, password);
			if (userDB == null) {
				message = "Echec lors de l'identification.";
			} else {
				message = "Bonjour " + userDB.getPseudo()
						+ ". Vous êtes connecté.";
				operationReussie = true;
				bundle.put(Bundle.USER, userDB);
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, operationReussie);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.USER, userDB);
	}
	
	@Author( auteur="Hainaut")
	@Version
	@Override
	public void ajouterUser(Bundle bundle) {
		boolean ajoutReussi = false;
		User userDB = null;
		String message;
		User user = (User) bundle.get(Bundle.USER);
		if (user == null) {
			message = "Inscription impossible : pas d'utilisateur.";
		} else if(user.getEmail() == null || user.getEmail().isEmpty() || !validEmail(user.getEmail())){
			message = "Inscription impossible : email invalide";
		}else if(user.getPseudo() == null || user.getPseudo().isEmpty()){
			message = "Inscription impossible : il manque un pseudo";
		} else if(user.getPassword() == null || validPaswd(user.getPassword())){
			message = "Inscription impossible : il faut au moins "+GestionUsersImpl.MDP+" caractères pour le mot de passe";
		}
		else {
			String email = user.getEmail();
			userDB = this.userDao.getUser(email);
			if (userDB != null) {
				message = "Inscription impossible, cette adresse email est déjà utilisée";
			} else {
				user.setImage(Bundle.REPERTOIRE_IMAGE+"Default.jpg");
				ajoutReussi = this.userDao.ajouterUser(user);
				
				if (ajoutReussi) {
					message = "Inscription réussie, vous pouvez vous connecter.";
				} else {
					message = "Une erreur inattendue est survenue.";
				}
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.MESSAGE, message);
		bundle.put(Bundle.USER, user);
	}
	
	private boolean validEmail(String email) {
		return  Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$").matcher(email.toUpperCase()).matches();
	}

	@Author( auteur="Hainaut")
	@Version
	private boolean validPaswd(String password) {
		return password.length() < GestionUsersImpl.MDP;
	}

	/*
	 * v2 -> si on ne met pas de nouveau MDP ou pseudo c'est qu'on ne veut pas changer
	 * v3 -> si on n'apporte aucunne modif
	 */
	@Author( auteur="Hainaut")
	@Version(version = 3)
	@Override
	public void modifierUser(Bundle bundle) {
		boolean modificationReussie = false;
		User userDB = null;
		String message;
		User user = (User) bundle.get(Bundle.USER);
		
		if (user == null) {
			message = "Modification impossible : pas d'utilisateur.";
		} else {
			String email = user.getEmail();
			userDB = this.userDao.getUser(email);
			if (userDB == null) {
				message = "Modification impossible, cet utilisateur n'existe pas";
			}else if(user.getPassword() != null && !user.getPassword().isEmpty() && validPaswd(user.getPassword())) {
				message = "Modification impossible : il faut au moins "+GestionUsersImpl.MDP+" caractères pour le mot de passe";
			}else {
				if(user.getPseudo() == null || user.getPseudo().isEmpty()){
					user.setPseudo(userDB.getPseudo());
				}
				if(user.getPassword() == null || user.getPassword().isEmpty()){
					user.setPassword(userDB.getPassword());
				}
				if(user.getImage() == null){
					user.setImage(userDB.getImage());
				}
				modificationReussie = this.userDao.modifierUser(user);
				
				if (modificationReussie) {
					message = "Modification réussie";
				} else {
					message = "Une erreur inattendue est survenue.";
				}
			}
		}
		bundle.put(Bundle.OPERATION_REUSSIE, modificationReussie);
		bundle.put(Bundle.MESSAGE, message);
	}
		
}
