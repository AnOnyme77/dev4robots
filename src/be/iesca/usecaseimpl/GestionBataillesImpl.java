package be.iesca.usecaseimpl;

import java.io.IOException;
import java.util.ArrayList;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.dao.BatailleDao;
import be.iesca.dao.RobotDao;
import be.iesca.dao.UserDao;
import be.iesca.daoimpl.BatailleDaoImpl;
import be.iesca.daoimpl.RobotDaoImpl;
import be.iesca.daoimpl.UserDaoImpl;
import be.iesca.domaine.Bataille;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.ThreadBataille;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionBatailles;
import be.iesca.util.Author;
import be.iesca.util.Fichier;
import be.iesca.util.Version;

public class GestionBataillesImpl implements GestionBatailles {

	private BatailleDao batailleDao;
	private UserDao userDao;
	private RobotDao robotDao;
	
	public GestionBataillesImpl(){
		this.batailleDao = new BatailleDaoImpl();
		this.userDao =  new UserDaoImpl();
		this.robotDao = new RobotDaoImpl();
	}
	
	@Author(auteur="Jamar")
	@Version(version = 1)
	@Override
	public void ajouterBataille(Bundle bundle) {
		Bataille bataille = (Bataille) bundle.get(Bundle.BATAILLE);
		boolean ajoutReussi = false;
		String message = "Echec lors de l'ajout de la bataille.";
		//Vérification des objets
		if(bataille != null){
			if(this.batailleDao.recupererBataille(bataille)==null){
				if(bataille.listeRobots().length >= 1){
					//On vérifie si le user existe
					if(bataille.getEmail() != null){
						//On test l'ajout
						ajoutReussi = this.batailleDao.ajouterBataille(bataille);
						if(ajoutReussi)
							message = "Ajout de la bataille "+bataille.getNom()+" effectué avec succès.";
						//S'il réussi on spécifie un message de succès.
					}
					else
						message = "Email de la bataille incorrect.";
				}
				else
					message = "Il vous faut au moins un robot dans la bataille "+bataille.getNom()+".";
			}else
				message = "La bataille "+bataille.getNom()+" existe déjà.";
		}
		else
			message = "Opération échouée.";
		
		
		bundle.put(Bundle.OPERATION_REUSSIE, ajoutReussi);
		bundle.put(Bundle.MESSAGE, message);
	}

	@Author(auteur="Evrard")
	@Version(version = 1)
	@Override
	public void listerBataille(Bundle bundle) {
		Bataille bataille = (Bataille)bundle.get(Bundle.BATAILLE);
		ArrayList<Bataille> listeBataille = this.batailleDao.listerBataille(bataille);
		
		bundle.put(Bundle.OPERATION_REUSSIE, false);
		bundle.put(Bundle.MESSAGE, "Une erreur est survenue");
		if(listeBataille != null){
			bundle.put(Bundle.OPERATION_REUSSIE, true);
			bundle.put(Bundle.LISTE, listeBataille);
			if(listeBataille.size()>0)
				bundle.put(Bundle.MESSAGE, "Opération effectuée avec succès");
			else
				bundle.put(Bundle.MESSAGE, "La liste est vide");
		}
		
	}

	@Author(auteur="Lammens")
	@Version(version = 1)
	@Override
	public void modifierBataille(Bundle bundle) {
		Bataille bataille = (Bataille) bundle.get(Bundle.BATAILLE);
		String message = "";
		boolean modifReussie = false;
		if(bataille != null)
		{
			//verif si la bataille contient plusieur robot
			if(bataille.listeRobots().length >= 1)
			{
				//Verif si la bataille existe
				if(this.batailleDao.recupererBataille(bataille) != null)
				{
					//On vérifie si le user existe
					if(this.userDao.getUser(bataille.getEmail()) != null)
					{
						modifReussie = this.batailleDao.modifierBataille(bataille);
						if(modifReussie)
							message="Modification de la bataille "+bataille.getNom()+" effectuée avec succès !";
						else
							message="Opération échouée.";
					}
					else
						message = "Utilisateur inconnu";
				}
				else
					message="La bataille n'existe pas.";
			}
			else
				message = "Il n'y a aucun robot dans la liste";
		}
		else
		{
			message = "Aucune bataille passée en paramètre.";
		}
		bundle.put(Bundle.OPERATION_REUSSIE, modifReussie);
		bundle.put(Bundle.MESSAGE, message);
	}

	@Author(auteur="Evrard")
	@Version(version = 1)
	public void lancerBataille(Bundle bundle) {
		Bataille bataille=this.batailleDao.recupererBataille((Bataille)bundle.get(Bundle.BATAILLE));
		if(bataille!=null && bataille.listeRobots().length>=1){
			String [] listeRobots = new String[bataille.listeRobots().length];
			for(int i=0;i<bataille.listeRobots().length;i++)
				listeRobots[i]=bataille.listeRobots()[i]+".java";
			String sourcePath = Bundle.REPERTOIRE_ROBOTS;
			
			System.out.println("Recuperation des robots");
			try {
				Fichier.supprimerFichiers(Bundle.REPERTOIRE_ROBOTS);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			User us = GestionnaireUseCases.getInstance().getUser();
			for(String nomRobot : bataille.listeRobots()){
				ClasseRobot cl = new ClasseRobot();
				cl.setNom(nomRobot);				
				
				this.robotDao.getRobot(cl, us);
			}
			System.out.println("Fin de la recuperation des robots");
			
			ThreadBataille thBataille = new ThreadBataille(sourcePath,listeRobots);
			thBataille.start();
		}
		else{
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE, "Opération échouée");
		}
	}

	@Author(auteur="Evrard")
	@Version(version = 1)
	public void recupererBataille(Bundle bundle) {
		Bataille bataille=this.batailleDao.recupererBataille((Bataille)bundle.get(Bundle.BATAILLE));
		if(bataille!=null){
			bundle.put(Bundle.OPERATION_REUSSIE, true);
			bundle.put(Bundle.MESSAGE, "Récupération effectuée avec succès");
			bundle.put(Bundle.BATAILLE, bataille);
		}
		else{
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE, "Opération échouée");
		}
	}
	
	@Author(auteur="Hainaut")
	@Version(version = 2)
	@Override
	public void supprimerBataille(Bundle bundle) {
		Bataille bataille = (Bataille) bundle.get(Bundle.BATAILLE);
		String message = "";
		boolean suppressionReussie = false;
		if(bataille!=null){
			suppressionReussie = this.batailleDao.supprimerBataille(bataille);
			if (suppressionReussie)
				message = "Suppression de la bataille "+bataille.getNom()+" réalisée avec succès";
			else
				message = "Opération échouée";
		}
		else
			message = "Pas de bataille à supprimer";
		bundle.put(Bundle.OPERATION_REUSSIE, suppressionReussie);
		bundle.put(Bundle.MESSAGE, message);
		
	}

}
