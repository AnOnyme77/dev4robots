package be.iesca.usecaseimpl;

import java.util.ArrayList;

import be.iesca.dao.RobotDao;
import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionRobots;
import be.iesca.util.Author;
import be.iesca.util.Version;

public class GestionRobotsImpl implements GestionRobots {

	private UserDao userDao;
	private RobotDao robotDao;

	@Author(auteur = "Evrard")
	@Version(version = 1)
	public GestionRobotsImpl() {
		this.userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(
				UserDao.class);
		this.robotDao = (RobotDao) DaoFactory.getInstance().getDaoImpl(
				RobotDao.class);
	}

	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Override
	public void ajouterRobot(Bundle bundle) {
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		User user = (User) bundle.get(Bundle.USER);
		boolean existant;
		boolean aRobot;
		if (user != null && robot != null) {
			if (this.userDao.getUser(user.getEmail()) != null) {
				existant = this.robotDao.robotExist(robot);
				aRobot = this.robotDao.robotUserExist(robot, user);
				if (!existant) {
					if (this.robotDao.ajouterRobot(robot, user)
							&& this.robotDao.ajouterUserRobot(robot, user)) {
						bundle.put(Bundle.OPERATION_REUSSIE, true);
						bundle.put(Bundle.MESSAGE,
								"Opération effectuée avec succès");
					} else {
						bundle.put(Bundle.OPERATION_REUSSIE, false);
						bundle.put(Bundle.MESSAGE, "Une erreur s'est produite");
					}
				} else {
					if (!aRobot) {
						if (this.robotDao.ajouterUserRobot(robot, user)) {
							bundle.put(Bundle.OPERATION_REUSSIE, true);
							bundle.put(Bundle.MESSAGE,
									"Opération effectuée avec succès");
						} else {
							bundle.put(Bundle.OPERATION_REUSSIE, false);
							bundle.put(Bundle.MESSAGE,
									"Une erreur s'est produite");
						}
					} else {
						bundle.put(Bundle.OPERATION_REUSSIE, false);
						bundle.put(Bundle.MESSAGE,
								"Vous possédez déjà ce robot");
					}
				}
			} else {
				bundle.put(Bundle.OPERATION_REUSSIE, false);
				bundle.put(Bundle.MESSAGE, "Cet utilisateur n'existe pas");
			}
		} else {
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE,
					"Les informations reçues sont incorrectes");
		}

	}

	@Author(auteur = "Jamar")
	@Version(version = 1)
	@Override
	public void supprimerRobot(Bundle bundle) {
		ClasseRobot robot = (ClasseRobot) bundle.get(Bundle.ROBOT);
		User user = (User) bundle.get(Bundle.USER);
		if (user != null && robot != null) {
			if (this.userDao.getUser(user.getEmail()) != null) {
				if (this.robotDao.supprimerUserRobot(robot, user)) {
					// Si le robot n'est plus utilisé, on peut le supprimer dans
					// la table robots
					if (!this.robotDao.robotIsUsed(robot))
						this.robotDao.supprimerRobot(robot, user);
					bundle.put(Bundle.OPERATION_REUSSIE, true);
					bundle.put(Bundle.MESSAGE, "Le robot " + robot.getNom()
							+ " supprimé avec succès");
				} else {
					bundle.put(Bundle.OPERATION_REUSSIE, false);
					bundle.put(
							Bundle.MESSAGE,
							"Impossible de supprimer le robot "
									+ robot.getNom() + ".");
				}
			} else {
				bundle.put(Bundle.OPERATION_REUSSIE, false);
				bundle.put(Bundle.MESSAGE, "L'utilisateur " + user.getPseudo()
						+ " n'existe pas");
			}
		} else {
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE,
					"Les informations reçues sont incorrectes.");
		}
	}

	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Override
	public void listerRobots(Bundle bundle) {
		ArrayList<ClasseRobot> liste = this.robotDao.listerRobots();
		if (liste != null) {
			if (liste.size() > 0) {
				bundle.put(Bundle.OPERATION_REUSSIE, true);
				bundle.put(Bundle.MESSAGE, "Opération effectuée avec succès");
				bundle.put(Bundle.LISTE, liste);
			} else {
				bundle.put(Bundle.OPERATION_REUSSIE, true);
				bundle.put(Bundle.MESSAGE, "La liste est vide");
			}
		} else {
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE,
					"Une erreur s'est produite lors de la récupération");
		}

	}

	@Author(auteur = "Evrard")
	@Version(version = 1)
	public void listerRobotsUser(Bundle bundle) {

		User user = (User) bundle.get(Bundle.USER);
		ArrayList<ClasseRobot> liste = this.robotDao.listerRobotsUser(user);
		if (liste != null) {
			if (liste.size() > 0) {
				bundle.put(Bundle.OPERATION_REUSSIE, true);
				bundle.put(Bundle.MESSAGE,
						"Récupération effectuée avec succès.");
				bundle.put(Bundle.LISTE, liste);
			} else {
				bundle.put(Bundle.OPERATION_REUSSIE, true);
				bundle.put(Bundle.MESSAGE,
						"Vous n'avez encore aucun robot dans votre liste.");
				bundle.put(Bundle.LISTE, liste);
			}
		} else {
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE,
					"Une erreur s'est produite lors de la récupération.");
		}
	}

	@Override
	public void viderRobots(Bundle bundle) {
		/*
		 * Permet de vider tous les robots du USER !! Il faut faire une boucle
		 * foreach : Tant qu'il y a des robots dans la liste du User, on appelle
		 * supprimerUserRobot et supprimerRobot.
		 */
		boolean operationSuppUser = false;
		boolean operationSupp = false;
		boolean isUsed = true;
		User utilisateur = (User) bundle.get(Bundle.USER);
		ArrayList<ClasseRobot> liste = this.robotDao
				.listerRobotsUser(utilisateur);
		if (liste != null) {
			// Vérification si la liste est pleine
			if (liste.size() > 0) {
				// Pour chaque robot dans la liste,
				// ------ DEBUT FOREACH ------
				for (ClasseRobot robot : liste) {
					// On regarde s'il est utilisé par un autre user
					isUsed = this.robotDao.robotIsUsed(robot);
					if (!isUsed) {
						// S'il n'est pas utilisé, on peut le supprimer dans la
						// table robots
						operationSuppUser = this.robotDao.supprimerUserRobot(
								robot, utilisateur);
						if (operationSuppUser)
							operationSupp = this.robotDao.supprimerRobot(robot,
									utilisateur);
					} else {
						// Sinon, on le supprime juste dans la table userRobots
						operationSuppUser = this.robotDao.supprimerUserRobot(
								robot, utilisateur);
					}

				}
				// ------ FIN FOREACH ------
				// On vérifie si les suppressions se sont bien passées :
				if (operationSupp && operationSuppUser) {
					bundle.put(Bundle.OPERATION_REUSSIE, true);
					bundle.put(Bundle.MESSAGE,
							"Suppressions effectuées avec succès.");
				} else {
					bundle.put(Bundle.OPERATION_REUSSIE, false);
					bundle.put(Bundle.MESSAGE,
							"Erreur lors de la suppression de vos robots");
				}
			} else {
				bundle.put(Bundle.OPERATION_REUSSIE, false);
				bundle.put(Bundle.MESSAGE,
						"Vous n'avez encore aucun robot dans votre liste.");
			}
		} else {
			bundle.put(Bundle.OPERATION_REUSSIE, false);
			bundle.put(Bundle.MESSAGE,
					"Une erreur s'est produite lors de la récupération.");
		}

	}

}
