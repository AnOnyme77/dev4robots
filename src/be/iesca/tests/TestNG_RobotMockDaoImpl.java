package be.iesca.tests;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.RobotDao;
import be.iesca.daoimpl.RobotMockDaoImpl;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Fichier;
import be.iesca.util.Version;

@Author(auteur="Jamar",reviseur={"Evrard"})
@Version(version=1)
public class TestNG_RobotMockDaoImpl 
{
	/*
	 * J'adore les tests :D
	 */
	private List<ClasseRobot> robots;
	private User user = new User("dev4Robot@me.com", "dev4Robot@me.com");
	protected RobotDao robotDao;
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@BeforeClass
	/*Initialisation du RobotMockDaoImpl pour avoir acces aux m��thodes de gestion d'un robot*/
	public void initDao() 
	{
		this.robotDao = new RobotMockDaoImpl();
	}
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@BeforeClass
	public void initialiserListeRobots() 
	{
		robots = new ArrayList<ClasseRobot>();
		String chaineRobots[] = {"Corners","Crazy","Fire","RamFire"};
		/*Décoration d'une liste de robots pour la manipulation des tests*/
		for(int i=0; i<chaineRobots.length; i++)
		{
			ClasseRobot robot;
			try {
				
				robot = new ClasseRobot(chaineRobots[i],Bundle.REPERTOIRE_ROBOTS);

				robots.add(robot);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@Test
	public void testAjouterRobot()
	{
		try {
			Fichier.supprimerFichiers("./bin");
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(ClasseRobot currentRobot: this.robots)
			//Doit retourner Vrai pour l'ajout de tous les robots qui se trouvent dans la liste robots.
			//Il y a en a 4.
			assertTrue(this.robotDao.ajouterRobot(currentRobot, this.user));
	}
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@Test
	public void testListerRobots()
	{
		ArrayList <ClasseRobot> maListe = new ArrayList<ClasseRobot>();
		maListe = this.robotDao.listerRobots();
		//On a 4 robots dans la liste, elle ne peut pas etre égale à 0
		//Doit retourner VRAI car liste contient 4 robots
		assertTrue(maListe.size() != 0);
		//Doit retourner VRAI, car la liste ne peut etre vide.
		assertTrue(!maListe.isEmpty());
		
		for(ClasseRobot currentRobot : maListe)
			//Les robots ne sont pas null, alors chaque robot doit retourner VRAI.
			assertTrue(currentRobot!=null);
	}
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@Test
	public void testSupprimerRobot()
	{
		//Doit retourner VRAI. Les robots existent donc on peut les supprimer.
		for(ClasseRobot currentRobot : this.robots)
			assertTrue(this.robotDao.supprimerRobot(currentRobot, this.user));
	}
	
}
