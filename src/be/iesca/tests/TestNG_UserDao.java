package be.iesca.tests;

import static org.testng.AssertJUnit.*;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.testng.annotations.Test;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;

/*
 * Tests unitaires de RobotDao
 */
public class TestNG_UserDao {
	
	private static final User user1 = new User("toto@hotmail.com", "toto", "1234", "./ImageProfil/Default.jpg");
	private static final User user2 = new User("titi@hotmail.com", "titi", "4321", "./ImageProfil/Default.jpg");
	private static final User user3 = new User("tata@hotmail.com", "tata", "coucou", "./ImageProfil/Default.jpg");

	private UserDao userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(
			UserDao.class);
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test
	public void testajout() 
	{
		assertTrue(userDao.ajouterUser(user1));
		assertTrue(userDao.ajouterUser(user2));
		assertTrue(userDao.ajouterUser(user3));
		assertFalse(userDao.ajouterUser(user1));
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testajout" })
	public void testExist()
	{
		assertNotNull(userDao.getUser(user1.getEmail()).getEmail());
		assertNotNull(userDao.getUser(user2.getEmail()).getEmail());
		assertNotNull(userDao.getUser(user3.getEmail()).getEmail());
		assertNull(userDao.getUser("tutu@hotmail.com"));
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testExist" })
	public void testGet()
	{
		assertNotNull(userDao.getUser(user1.getEmail(), user1.getPassword()));
		assertNotNull(userDao.getUser(user2.getEmail(), user2.getPassword()));
		assertNotNull(userDao.getUser(user3.getEmail(), user3.getPassword()));
		assertNull(userDao.getUser(user1.getEmail(), user2.getPassword()));
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testGet" })
	public void testModif()
	{
		user1.setPseudo("toto2");
		assertTrue(userDao.modifierUser(user1));
		user2.setPseudo("titi2");
		assertTrue(userDao.modifierUser(user2));
		user3.setPseudo("tata2");
		assertTrue(userDao.modifierUser(user3));
	}
	
	// suppression des enregistrement
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testModif" })
	public void Suppression_champs()
	{
		boolean retour=false;
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement("delete from users where email='titi@hotmail.com';" 
										+"delete from users where email='tata@hotmail.com';" 
										+"delete from users where email='toto@hotmail.com';");
			int resultat = ps.executeUpdate();
			if (resultat == 1) 
				retour = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			//cloture
			try 
			{
				if (ps != null)
					ps.close();
			} 
			catch (Exception ex) {}
			try 
			{
				if (con != null)
					con.close();
			} catch (Exception ex) {}
		}
		
		assertTrue(retour);
	}


}
