package be.iesca.tests;

import be.iesca.ihm.LancementBataille;

public class PgmTestLancementBataille {
	/**
	 * Pgm de test du lancement d'une bataille
	 * Les 3 fichiers robots doivent �tre pr�sents dans ./robots/test-robots/ 
	 * Ils vont �tre copi�s et compil�s dans le r�pertoire ./robots/robots/
	 * Les fichiers .java et .class des robots doivent �tre dans ce r�pertoire apr�s la compilation. 
	 * Ceci est n�cessaire pour que Robocode puisse lancer la bataille.
	 * Si tout va bien, Robocode d�marre automatiquement et lance la bataille avec les robots
	 * pr�sents dans ce r�pertoire. 
	 * Le code source des robots doit commencer par : package robots;
	 */
	public static void main(String[] args) {
		String[] nomRobots = {"Corners.java", "Crazy.java", "Fire.java"};
		String sourcePath = System.getProperty("user.dir") + "/robots/test-LancementBataille/";

		LancementBataille lb = new LancementBataille(sourcePath, nomRobots);
		lb.demarrer();

	}
}
