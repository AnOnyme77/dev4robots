package be.iesca.tests;

import static org.testng.AssertJUnit.*;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.UserMockDaoImpl;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;


public class TestNG_UserMockDaoImpl {
	private List<User> users;
	protected UserDao userDao;

	@Author( auteur="Lammens")
	@Version(version = 1)
	@BeforeClass
	// sera exécuté avant toutes les méthodes
	public void initialiserLeDao() {
		this.userDao = new UserMockDaoImpl();
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@BeforeClass
	// sera exécuté avant toutes les méthodes
	public void initialiserListeUsers() {
		users = new ArrayList<User>(6);
		// ajout des users
		for(int i=0; i<6; i++)
		{
			User user = new User("Toto"+i+"@hotmail.com", "toto"+i,"kikou"+i, "./ImageProfil/Default.jpg");
			users.add(user);
			
		}
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test
	public void TestAjouter()
	{
		for(User u: this.users)
			assertTrue(this.userDao.ajouterUser(u));
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "TestAjouter" })
	public void TestExist()
	{
		for(User u: this.users)
			assertEquals(u, this.userDao.getUser(u.getEmail()));
		assertEquals(null,this.userDao.getUser("titti@hotmail.com"));
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "TestExist" })
	public void TestConnection()
	{
		for(User u: this.users)
			assertEquals(u, this.userDao.getUser(u.getEmail(), u.getPassword()));
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods = { "TestConnection" })
	public void TestModifier()
	{
		User MonUser = users.get(0);
		MonUser.setPassword("coucou");
		MonUser.setPseudo("cacahuete");
		assertTrue(this.userDao.modifierUser(MonUser));
		assertEquals(MonUser, userDao.getUser(MonUser.getEmail()));	
		
	}
	
}
