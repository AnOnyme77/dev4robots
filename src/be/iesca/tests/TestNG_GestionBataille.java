package be.iesca.tests;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bataille;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionBatailles;
import be.iesca.usecase.GestionRobots;
import be.iesca.usecaseimpl.GestionBataillesImpl;
import be.iesca.usecaseimpl.GestionRobotsImpl;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author(auteur="Jamar")
@Version
public class TestNG_GestionBataille {
	
	//Les listes :
	private ArrayList<String> listeRobots = null;
	private ArrayList<User> listeUsers = null;
	private ArrayList<Bataille> listeBatailles = null;
	
	//Les dao :
	private UserDao userDao;
	
	//Les useCases :
	private GestionBatailles gestionBatailles;
	private GestionRobots gestionRobots;
	
	//Autres variables pour les test :
	private Bundle bundle;
	
	public TestNG_GestionBataille() {
		this.userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(UserDao.class);
		this.gestionBatailles = new GestionBataillesImpl();
		this.gestionRobots = new GestionRobotsImpl();
		this.bundle = new Bundle();
	}
	
	/**
	 * 4 Users en base de données !
	 * */
	@Author(auteur="Jamar")
	@Version(version=1)
	@BeforeClass
	/*Initialisation d'une liste de users pour les tests*/
	public void initListeUsers() {
		this.listeUsers = new ArrayList<User>(4);
		// ajout des users
		for(int i=0; i<4; i++){
			User user = new User("User"+i+"@robot.com", "iUser"+i,"motdepasse"+i, "./ImageProfil/Default.jpg");
			this.userDao.ajouterUser(user);
			this.listeUsers.add(user);
		}
	}
	
	
	/**
	 * 4 Robots en base de données !
	 * */
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@BeforeClass(dependsOnMethods={"initListeUsers"})
	public void initialiserListeRobots() 
	{
		this.listeRobots = new ArrayList<String>();
		String chaineRobots[] = {"SittingDuck","Crazy","Fire","RamFire"};
		/*Décoration d'une liste de robots pour la manipulation des tests*/
		for(int i=0; i<chaineRobots.length; i++)
		{
			ClasseRobot robot;
			try {
				
				robot = new ClasseRobot(chaineRobots[i],Bundle.REPERTOIRE_ROBOTS);
				this.bundle.put(Bundle.ROBOT, robot);
				this.bundle.put(Bundle.USER, this.listeUsers.get(i));
				this.gestionRobots.ajouterRobot(this.bundle);
				this.listeRobots.add(robot.getNom());
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * 4 Batailles !
	 * */
	@Author(auteur="Jamar")
	@Version(version=1)
	@BeforeClass(dependsOnMethods={"initialiserListeRobots"})
	/*Initialisation d'une liste de batailles pour les tests*/
	public void initListeBatailles() {
		this.listeBatailles = new ArrayList<Bataille>(4);
		// ajout des batailles
		for(int i=0; i<4; i++){
			Bataille bataille = new Bataille();
			bataille.setEmail("User"+i+"@robot.com");
			bataille.setNom("Sparta"+i);
			bataille.ajouterRobot(this.listeRobots.get(i));
			this.listeBatailles.add(bataille);
		}
	}
	
	
	/**
	 * Fonction de tests :
	 * */
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@Test
	public void testAjoutBataille(){
		for(int i=0; i<4; i++){
			//On va ajouter 4 batailles différentes dans la DB.
			this.bundle.put(Bundle.BATAILLE, this.listeBatailles.get(i));
			this.gestionBatailles.ajouterBataille(this.bundle);
			assertTrue((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		}
		
		//Ajout d'une bataille nulle pour tester si la fonction retourne bien faux.
		this.bundle.put(Bundle.BATAILLE, null);
		this.gestionBatailles.ajouterBataille(this.bundle);
		assertFalse((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		
		//Ajout de nouveau d'une bataille qui se trouve déjà en DB. Une bataille exact sur tous les id.
		//Au hasard ... la 1ere 
		this.bundle.put(Bundle.BATAILLE, this.listeBatailles.get(0));
		this.gestionBatailles.ajouterBataille(this.bundle);
		assertFalse((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
	}
	
	@SuppressWarnings("unchecked")
	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Test(dependsOnMethods={"testAjoutBataille"})
	public void testListerBataille(){
		Bataille bataille = new Bataille();
		bataille.setEmail(this.listeUsers.get(1).getEmail());
		
		this.bundle.put(Bundle.BATAILLE, bataille);
		this.gestionBatailles.listerBataille(this.bundle);
		
		/*On vérifie que l'opération s'est bien déroulée*/
		assertTrue((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		
		/*On vérifie que la liste n'est pas vide*/
		assertTrue(((ArrayList<Bataille>)this.bundle.get(Bundle.LISTE)).size()>0);
		
		/*On vérifie que chaque est bien présente dans la liste des batailles*/
		for(Bataille batailleActuelle:(ArrayList<Bataille>)this.bundle.get(Bundle.LISTE)){
			assertTrue(this.listeBatailles.contains(batailleActuelle));
			/*On vérifie que la bataille contient bien les robots*/
			for(String robot: batailleActuelle.listeRobots()){
				assertTrue(this.listeRobots.contains(robot));
			}
		}
	}
	
	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Test(dependsOnMethods={"testListerBataille"})
	public void testrecupererBataille(){
		Bataille bataille = new Bataille();
		bataille.setEmail(this.listeBatailles.get(1).getEmail());
		bataille.setNom(this.listeBatailles.get(1).getNom());
		
		this.bundle.put(Bundle.BATAILLE, bataille);
		this.gestionBatailles.recupererBataille(this.bundle);
		
		/*On vérifie que l'opération s'est bien déroulée*/
		assertTrue((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		
		/*On vérifie que la bataille n'est pas nulle*/
		assertNotNull(this.bundle.get(Bundle.BATAILLE));
		
		/*On vérifie que la bataille contient bien ses robots*/
		for(String robot: ((Bataille)this.bundle.get(Bundle.BATAILLE)).listeRobots()){
			assertTrue(this.listeRobots.contains(robot));
		}
		
	}
	
	@Author(auteur = "Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods={"testrecupererBataille"})
	public void testModifBataille()
	{
		// demande de modif en passant une bataille null
		this.bundle.put(Bundle.BATAILLE, null);
		this.gestionBatailles.modifierBataille(bundle);
		assertFalse((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		
		// modif d'une bataille inexistante
		Bataille batailleUseless = new Bataille();
		batailleUseless.setNom("This is pas ca !");
		batailleUseless.setEmail(this.listeUsers.get(0).getEmail());
		this.bundle.put(Bundle.BATAILLE, batailleUseless);
		this.gestionBatailles.modifierBataille(bundle);
		assertFalse((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		
		// modif avec un robot qui n'est pas en DB
		for(Bataille bataille: this.listeBatailles){
			bataille.ajouterRobot("Trololol");
			this.bundle.put(Bundle.BATAILLE, bataille);
			this.gestionBatailles.modifierBataille(bundle);
			assertFalse((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
			bataille.supprimerRobot("Trololol");
		}
		
		// modif d'une bataille en ajoutant un robot
		for(Bataille bataille: this.listeBatailles){
			bataille.ajouterRobot("Fire");
			this.bundle.put(Bundle.BATAILLE, bataille);
			this.gestionBatailles.modifierBataille(bundle);
			assertTrue((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		}
		
		// modif d'une bataille en supprimant un robot
		this.listeBatailles.get(0).supprimerRobot("Fire");
		this.bundle.put(Bundle.BATAILLE, this.listeBatailles.get(0));
		this.gestionBatailles.modifierBataille(bundle);
		assertTrue((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		
	}
	
	/*
	 * suppression de toutes les batailles
	 * doit retourné true pour l'opération et false après si on la cherche
	 */
	@Author(auteur = "Hainaut", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods={"testModifBataille"})
	public void testSuppression1(){
		for(Bataille bataille: this.listeBatailles){
			this.bundle.put(Bundle.BATAILLE, bataille);
			this.gestionBatailles.supprimerBataille(bundle);
			assertTrue((boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
		}
			
	}

	/*
	 * suppression d'une bataille null -> doit retourner false
	 */
	@Author(auteur = "Hainaut", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods={"testModifBataille"})
	public void testSuppression2(){

		this.bundle.put(Bundle.BATAILLE, null);
		this.gestionBatailles.supprimerBataille(bundle);
		assertTrue(!(boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
	}
	
	/*
	 * suppression d'une bataille déjà supprimée (donc une bataille inconnue)
	 * doit retourner false
	 * remarque: permet de vérifier si le test "testSuppression1" a bien supprimer la bataille.
	 */
	@Author(auteur = "Hainaut", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods={"testSuppression1"})
	public void testSuppression3(){
		this.bundle.put(Bundle.BATAILLE, this.listeBatailles.get(0));
		this.gestionBatailles.supprimerBataille(bundle);
		assertTrue(!(boolean)this.bundle.get(Bundle.OPERATION_REUSSIE));
	}
}
