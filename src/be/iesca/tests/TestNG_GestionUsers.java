package be.iesca.tests;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionUsers;
import be.iesca.usecaseimpl.GestionUsersImpl;
import be.iesca.util.Author;
import be.iesca.util.Version;

/*
 * Tests fonctionnels du usecase GestionUsers
 */

@Author(auteur = "Legrand", reviseur = { "Hainaut" })
@Version
public class TestNG_GestionUsers {
	private ArrayList<User> users;

	private GestionUsers gestionUsers;
	private Bundle bundle;

	@BeforeClass
	public void initialisation() {
		this.bundle = new Bundle();
		this.gestionUsers = new GestionUsersImpl();
	}

	@BeforeClass
	public void initialiserListeUser() {
		users = new ArrayList<User>(3);
		// ajout des users
		for (int i = 0; i < 3; i++) {
			User user = new User("email" + i + "@email.com",
					"motdepasseassezlong" + i);
			user.setPseudo("monPseudo" + i);
			users.add(user);
		}
	}

	/*
	 * ajout des 3 utilisateurs mise dans la liste "users" dois retourner true
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test
	public void testAjout1() {
		for (int i = 0; i < 3; i++) {
			bundle.put(Bundle.USER, this.users.get(i));
			this.gestionUsers.ajouterUser(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		}
	}

	/*
	 * différent ajout qui doivent retourner false
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testAjout2() {
		// si pas user
		bundle.vider();
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		// si user vide
		bundle.put(Bundle.USER, new User("", ""));
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		// si pas d'email
		User user = new User("", "motdepasseassezlong");
		user.setPseudo("monPseudo");
		bundle.put(Bundle.USER, user);
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		// si pas de pwd
		user = new User();
		user.setPseudo("monPseudo");
		user.setEmail("email@email.com");
		bundle.put(Bundle.USER, user);
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		// si email mais invalide
		user = new User();
		user.setPseudo("monPseudo");
		user.setEmail("elfk;qmf,");
		user.setPassword("motdepasseassezlong");
		bundle.put(Bundle.USER, user);
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		// si pwd trop court
		user = new User();
		user.setPseudo("monPseudo");
		user.setPassword("pwd");
		bundle.put(Bundle.USER, user);
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

		// si existe déjà
		bundle.put(Bundle.USER, this.users.get(0));
		this.gestionUsers.ajouterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

	/*
	 * tantative de récupération (connection) des différent users mis en db.
	 * dois retourné true et le user en db dois correspondre a celui de la liste
	 * "users"
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testConnecter1() {
		for (int i = 0; i < 3; i++) {
			bundle.put(Bundle.USER, this.users.get(i));
			this.gestionUsers.connecterUser(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
			User user = (User) bundle.get(Bundle.USER);
			assertNotNull(user);
			assertEquals(this.users.get(i).getPseudo(), user.getPseudo());
			assertEquals(this.users.get(i).getEmail(), user.getEmail());
			assertEquals(this.users.get(i).getPassword(), user.getPassword());

		}
	}

	/*
	 * tantative de récupération (connection) avec mauvais pseudo dois returner
	 * false
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testConnecter2() {
		User user = new User();
		user.setEmail("email@nepasLa");
		user.setPassword("pwdVrememennjndj");
		bundle.put(Bundle.USER, user);
		this.gestionUsers.connecterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

	/*
	 * tantative de récupération (connection) avec mauvais mot de passe dois
	 * returner false
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testConnecter4() {
		User user = new User();
		user.setEmail(this.users.get(0).getEmail());
		user.setPassword("motDepassPascorrect");
		bundle.put(Bundle.USER, user);
		this.gestionUsers.connecterUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
	}

	/*
	 * modification d'un user valide dois returner true
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testModifier1() {
		this.users.get(0).setPseudo("newPseudo");
		bundle.put(Bundle.USER, this.users.get(0));
		this.gestionUsers.modifierUser(bundle);
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

	}

	/*
	 * modif de email -> impossible dois returner false
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testModifier2() {
		this.users.get(0).setEmail("tentative@emailvrai.be");
		bundle.put(Bundle.USER, this.users.get(0));
		this.gestionUsers.modifierUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));

	}

	/*
	 * modif de pseudo a vide dois returner false
	 */
	@Author(auteur = "Hainaut")
	@Version
	@Test(dependsOnMethods = "testAjout1")
	public void testModifier3() {
		this.users.get(0).setPseudo("");
		bundle.put(Bundle.USER, this.users.get(0));
		this.gestionUsers.modifierUser(bundle);
		assertFalse((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		this.users.get(0).setPseudo("toto"); // remet un bon pseudo pour les
												// autres tests
	}

}