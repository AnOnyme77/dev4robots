package be.iesca.tests;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.RobotDao;
import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;

/*
 * Tests unitaires de UserDao
 * Ces tests supposent que la BD ne comprenne pas les users ci-dessous
 */
@Author(auteur="Jamar",reviseur={"Evrard"})
@Version(version=2)
public class TestNG_RobotDao {
	
	private static final User user1 = new User("toto@hotmail.com", "toto", "1234","");
	private static final User user2 = new User("titi@hotmail.com", "titi", "4321","");
	private static final User user3 = new User("tata@hotmail.com", "tata", "coucou","");
	private ArrayList<ClasseRobot> robots = null;
	private UserDao userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(UserDao.class);
	private RobotDao robotDao = (RobotDao) DaoFactory.getInstance().getDaoImpl(RobotDao.class);
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@BeforeClass
	public void initialiserListeRobots() 
	{
		robots = new ArrayList<ClasseRobot>();
		String chaineRobots[] = {"Corners","Crazy","Fire","RamFire"};
		/*Décoration d'une liste de robots pour la manipulation des tests*/
		for(int i=0; i<chaineRobots.length; i++)
		{
			ClasseRobot robot;
			try {
				
				robot = new ClasseRobot(chaineRobots[i],Bundle.REPERTOIRE_ROBOTS);

				robots.add(robot);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Test
	public void testajout() 
	{	
		//Ajout des users dans la DB
		//Doivent retourner VRAI car les 3 users sont initialisés au dessus.
		assertTrue(userDao.ajouterUser(user1));
		assertTrue(userDao.ajouterUser(user2));
		assertTrue(userDao.ajouterUser(user3));
		
		//Ajout de son robot1 :
		//Doit retourner VRAI, on ajoute le robot 1 au user1 et mise a jour dans la table userRobots
		assertTrue(robotDao.ajouterRobot(robots.get(0), user1));
		assertTrue(robotDao.ajouterUserRobot(robots.get(0), user1));
		//Ajout de son robot2 :
		//Doit retourner VRAI, on ajoute le robot 2 au user1 et mise a jour dans la table userRobots
		assertTrue(robotDao.ajouterRobot(robots.get(1), user1));
		assertTrue(robotDao.ajouterUserRobot(robots.get(1), user1));
		
		//Ajout du robot3 sur user2 :
		//Doit retourner VRAI, on ajoute le robot 3 au user2 et mise a jour dans la table userRobots
		assertTrue(robotDao.ajouterRobot(robots.get(2), user2));
		assertTrue(robotDao.ajouterUserRobot(robots.get(2), user2));
		
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testajout" })
	public void testExist()
	{
		//On vérifie le robot1 du user1 (tables : robots, userrobots)
		assertTrue(robotDao.robotExist(robots.get(0)));
		assertTrue(robotDao.robotUserExist(robots.get(0), user1));
		//On vérifie le robot2 du user1 (tables : robots, userrobots)
		assertTrue(robotDao.robotExist(robots.get(1)));
		assertTrue(robotDao.robotUserExist(robots.get(1), user1));
		
		//Essai sur la fonction robotIsUsed
		assertTrue(robotDao.robotIsUsed(robots.get(1)));
		
		//On vérifie le robot3 du user2 (tables : robots, userrobots)
		assertTrue(robotDao.robotExist(robots.get(2)));
		assertTrue(robotDao.robotUserExist(robots.get(2), user2));
		
		//FALSE STATEMENTS : vérifie s'il existe un robot 3 pour user1 (user2 a le robot3)
		assertFalse(robotDao.robotUserExist(robots.get(2), user1));
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testExist" })
	public void testGet()
	{
		//Essai de récupération des robots1 et 2 du user1
		assertNotNull(robotDao.getRobot(robots.get(0), user1));
		assertNotNull(robotDao.getRobot(robots.get(1), user1));
		//Essai de récupération du robot3 sur le user2
		assertNotNull(robotDao.getRobot(robots.get(2), user2));	
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testGet" })
	public void testLister()
	{
		//Essai de récupération des robots1 et 2 du user1
		assertNotNull(robotDao.listerRobotsUser(user1));
		
		//Essai de récupération du robot3 du user2
		assertNotNull(robotDao.listerRobotsUser(user2));
		
		//Essai de récupération de tous les robots :
		assertNotNull(robotDao.listerRobots());
		
		//FALSE STATEMENTS :
		//Essai de récupération de robots sur un user n'ayant pas de robots
		assertTrue(robotDao.listerRobotsUser(user3).isEmpty());
		
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Test(dependsOnMethods = { "testLister" })
	public void testSupp()
	{
		//Essai de suppression des robots1 et 2 du user1
		assertTrue(robotDao.supprimerUserRobot(robots.get(0), user1));
		assertTrue(robotDao.supprimerRobot(robots.get(0), user1));
		
		assertTrue(robotDao.supprimerUserRobot(robots.get(1), user1));
		assertTrue(robotDao.supprimerRobot(robots.get(1), user1));
		
		//Essai de suppression du robot3 du user 2
		assertTrue(robotDao.supprimerUserRobot(robots.get(2), user2));
		assertTrue(robotDao.supprimerRobot(robots.get(2), user2));
		
		//FALSE STATEMENTS : resuppression du robot1 du user1
		assertFalse(robotDao.supprimerUserRobot(robots.get(0), user1));
		assertFalse(robotDao.supprimerRobot(robots.get(0), user1));
		
	}
	
	// Suppression des users en DB
	@Author( auteur="Lammens", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods = { "testSupp" })
	public void Suppression_champs()
	{
		boolean retour=false;
		
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement("delete from users where email='titi@hotmail.com';"+
										"delete from users where email='tata@hotmail.com';"+
											"delete from users where email='toto@hotmail.com';");
			int resultat = ps.executeUpdate();
			if (resultat == 1) 
				retour = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			//cloture
			try 
			{
				if (ps != null)
					ps.close();
			} 
			catch (Exception ex) {}
			try 
			{
				if (con != null)
					con.close();
			} catch (Exception ex) {}
		}
		
		assertTrue(retour);
	}

}
