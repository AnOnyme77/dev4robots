package be.iesca.tests;

import static org.testng.AssertJUnit.*;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.BatailleDao;
import be.iesca.dao.UserDao;
import be.iesca.daoimpl.BatailleDaoImpl;
import be.iesca.daoimpl.UserDaoImpl;
import be.iesca.domaine.Bataille;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionRobots;
import be.iesca.usecaseimpl.GestionRobotsImpl;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author(auteur = "Evrard", reviseur={"Jamar"})
@Version(version = 1)
public class TestNG_BatailleDaoImpl {
	ArrayList<User> listeUsers;
	ArrayList<ClasseRobot> listeRobots;
	ArrayList<Bataille> listeBatailles;
	BatailleDao dao;
	UserDao userDao;
	GestionRobots gestionRobot;
	Bundle bundle;

	@Author(auteur = "Evrard")
	@Version(version = 1)
	public TestNG_BatailleDaoImpl() {
		this.listeRobots = new ArrayList<ClasseRobot>();
		this.listeUsers = new ArrayList<User>();
		this.listeBatailles = new ArrayList<Bataille>();
		this.dao = new BatailleDaoImpl();
		this.userDao = new UserDaoImpl();
		this.gestionRobot = new GestionRobotsImpl();
		this.bundle = new Bundle();
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@BeforeClass
	/*Initialisation d'une liste de users pour les tests*/
	public void initListeUsers() {
		this.listeUsers = new ArrayList<User>(4);
		// ajout des users
		for(int i=0; i<4; i++){
			User user = new User("User"+i+"@robot.com", "iUser"+i,"motdepasse"+i, "./ImageProfil/Default.jpg");
			this.userDao.ajouterUser(user);
			this.listeUsers.add(user);
		}
	}
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=1)
	@BeforeClass
	public void initialiserListeRobots() 
	{
		this.listeRobots = new ArrayList<ClasseRobot>();
		String chaineRobots[] = {"SittingDuck","Crazy","Fire","RamFire"};
		/*Décoration d'une liste de robots pour la manipulation des tests*/
		for(int i=0; i<chaineRobots.length; i++)
		{
			ClasseRobot robot;
			try {
				
				robot = new ClasseRobot(chaineRobots[i],Bundle.REPERTOIRE_ROBOTS);
				this.bundle.put(Bundle.ROBOT, robot);
				this.bundle.put(Bundle.USER, this.listeUsers.get(i));
				this.gestionRobot.ajouterRobot(this.bundle);
				this.listeRobots.add(robot);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@BeforeClass
	/*Initialisation d'une liste de batailles pour les tests*/
	public void initListeBatailles() {
		this.listeBatailles = new ArrayList<Bataille>(4);
		// ajout des batailles
		for(int i=0; i<4; i++){
			Bataille bataille = new Bataille();
			bataille.setEmail("User"+i+"@robot.com");
			bataille.setNom("Sparta"+i);
			this.listeBatailles.add(bataille);
		}
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@Test
	/*Initialisation d'une liste de batailles pour les tests*/
	public void testAjout() {
		for(Bataille currentBataille: this.listeBatailles){
			for(ClasseRobot currentRobot: this.listeRobots){
				//Comme j'ai des robots initialisés, je peux les ajouter dans une bataille
				//Doit retourner VRAI.
				assertTrue(currentBataille.ajouterRobot(currentRobot.getNom()));
			}
			assertTrue(this.dao.ajouterBataille(currentBataille));
		}
	}
	
	
	@Author(auteur="Evrard")
	@Version(version=1)
	@Test(dependsOnMethods={"testAjout"})
	//Test du listing des batailles
	public void testListerBataille() {
		ArrayList<Bataille> maListe = new ArrayList<Bataille>();
		maListe = this.dao.listerBataille(this.listeBatailles.get(0));
		//On vérifie si la liste est non vide
		assertTrue(maListe.size() != 0);
		assertTrue(!maListe.isEmpty());
		
		//On vérifie que chaque robot de la liste est bien initialisé
		for (Bataille currentRobot : maListe)
			assertTrue(currentRobot != null);
	}

	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Test(dependsOnMethods={"testListerBataille"})
	//Test sur la récupération des batailles
	public void testGetBataille() {
		//On récupère le robot
		Bataille bataille=this.listeBatailles.get(2);
		Bataille batailleRetour=this.dao.recupererBataille(bataille);
		
		//on vérifie ques les robots sont identiques
		assertEquals(bataille,batailleRetour);
		//On vérifie que la taille des listes est égale
		assertEquals(bataille.listeRobots().length,
				batailleRetour.listeRobots().length);
		//On vérifie que les robots sont égaux
		for(int i=0;i<bataille.listeRobots().length;i++){
			assertEquals(bataille.listeRobots()[i],batailleRetour.listeRobots()[i]);
		}
	}
	
	@Author(auteur = "Lammens")
	@Version(version = 1)
	@Test(dependsOnMethods={"testGetBataille"})
	public void testModifBataille() 
	{
		// suppression du robot fire et modification
		for(Bataille currentBataille: this.listeBatailles)
		{
			currentBataille.supprimerRobot("Fire");
			assertTrue(this.dao.modifierBataille(currentBataille));
		}
		
		// comparaison entre la bataille du get et la bataille de listeBataille
		for(Bataille currentBataille: this.listeBatailles)
		{
			String[] listeRobotCurrent = currentBataille.listeRobots();
			String[] listeRobot = this.dao.recupererBataille(currentBataille).listeRobots();
			
			for(int i = 0; i<listeRobotCurrent.length ;i++)
			{
				assertEquals(listeRobotCurrent[i],listeRobot[i]);
			}
		}
		
		// ajout du robot Fire et modification
		for(Bataille currentBataille: this.listeBatailles)
		{
			currentBataille.ajouterRobot("Fire");
			assertTrue(this.dao.modifierBataille(currentBataille));
		}
				
		// comparaison entre la bataille du get et la bataille de listeBataille
		for(Bataille currentBataille: this.listeBatailles)
		{
			String[] listeRobotCurrent = currentBataille.listeRobots();
			String[] listeRobot = this.dao.recupererBataille(currentBataille).listeRobots();
					
			for(int i = 0; i<listeRobotCurrent.length ;i++)
			{
				assertEquals(listeRobotCurrent[i],listeRobot[i]);
			}
		}
		
		// ajout d'un robot inexistant
		for(Bataille currentBataille: this.listeBatailles)
		{
			currentBataille.ajouterRobot("Trolol");
			assertFalse(this.dao.modifierBataille(currentBataille));
		}
	}
		
	/*
	 * suppression de toutes les batailles
	 * doit retourner true
	 */
	@Author(auteur = "Hainaut", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods={"testModifBataille"})
	public void testSuppression1(){
		for(Bataille bataille: this.listeBatailles)
			assertTrue(this.dao.supprimerBataille(bataille));
	}

	/*
	 * suppression d'une bataille null -> doit retourner false
	 */
	@Author(auteur = "Hainaut", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods={"testModifBataille"})
	public void testSuppression2(){
		assertTrue(!this.dao.supprimerBataille(null));
	}
	
	/*
	 * suppression d'une bataille déjà supprimée (donc une bataille inconnue)
	 * doit retourner false
	 */
	@Author(auteur = "Hainaut", reviseur={"Jamar"})
	@Version(version = 1)
	@Test(dependsOnMethods={"testSuppression1"})
	public void testSuppression3(){
		assertTrue(!this.dao.supprimerBataille(this.listeBatailles.get(0)));
	}
}
