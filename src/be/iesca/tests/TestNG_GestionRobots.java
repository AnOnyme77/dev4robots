package be.iesca.tests;
import static org.testng.AssertJUnit.*;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import be.iesca.dao.UserDao;
import be.iesca.daoimpl.DaoFactory;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.usecase.GestionRobots;
import be.iesca.usecaseimpl.GestionRobotsImpl;
import be.iesca.util.Author;
import be.iesca.util.Version;


/*
 * Tests fonctionnels du usecase GestionRobots
 */

@Author(auteur="Jamar")
@Version
public class TestNG_GestionRobots {
	
	private ArrayList<ClasseRobot> robots = null;
	private ArrayList<User> users = null;
	private UserDao userDao = (UserDao) DaoFactory.getInstance().getDaoImpl(UserDao.class);
	//private RobotDao robotDao = (RobotDao) DaoFactory.getInstance().getDaoImpl(RobotDao.class);
	private GestionRobots gestionRobots;
	private Bundle bundle;
	
	@BeforeClass
	public void initialisation() {
		this.bundle = new Bundle();
		this.gestionRobots = new GestionRobotsImpl();
	}
	
	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=2)
	@BeforeClass
	public void initialiserListeRobots() 
	{
		robots = new ArrayList<ClasseRobot>();
		String chaineRobots[] = {"Corners","Crazy","Fire","RamFire"};
		/*Décoration d'une liste de robots pour la manipulation des tests*/
		for(int i=0; i<chaineRobots.length; i++){
			ClasseRobot robot;
			try {
				robot = new ClasseRobot(chaineRobots[i],Bundle.REPERTOIRE_ROBOTS);
				robots.add(robot);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@BeforeClass
	public void initialiserListeUser() {
		users = new ArrayList<User>();
		/* Décoration d'une liste de users pour la manipulation des tests
		 * & passage des users en DB pour pouvoir leur assigner un robot.*/
		for(int i=0; i<4; i++){
			User user = new User("user"+i+"@user.com", "mdpuser"+i,"Pseudo"+i , "logo"+i);
			users.add(user);
			//Ajout du User courant en DB
			this.userDao.ajouterUser(user);
		}
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@Test
	public void testAjout(){
		//On passe 4 fois dans la boucle, on a 4 robots à ajouter.
		for(int i=0; i<4; i++){
			//Pour tous les robots se trouvant dans la liste, on va les ajouter en DB et mettre
			// la table userRobots a jour.
			bundle.put(Bundle.USER, this.users.get(i));
			bundle.put(Bundle.ROBOT,this.robots.get(i));
			this.gestionRobots.ajouterRobot(bundle);
			//On récupére l'opération reussie ou non dans le bundle.
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
			
		}
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@Test(dependsOnMethods = "testAjout")
	public void testListerRobots(){
		//On a 4 robots dans la DB, on doit en récupérer au moins 1 et Max 4.
		this.gestionRobots.listerRobots(bundle);
		//Op reussie du bundle.
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		//La liste ne doit pas etre nulle
		assertNotNull(bundle.get(Bundle.LISTE));
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@Test(dependsOnMethods = "testListerRobots")
	public void testListerRobotsUser(){
		//On a 4 robots dans la DB, on doit en récupérer au moins 1 
		//et Max 4 mais dans la table userRobots pour le USER1
		bundle.put(Bundle.USER, this.users.get(0));
		this.gestionRobots.listerRobotsUser(bundle);
		//On récupére l'opération reussie ou non dans le bundle.
		assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
		//La liste ne doit pas etre nulle
		assertNotNull(bundle.get(Bundle.LISTE));
	}
	
	@Author(auteur="Jamar")
	@Version(version=1)
	@Test(dependsOnMethods = "testListerRobotsUser")
	public void testSupprimerRobots(){
		for(int i=0; i<4; i++){
			//On supprime les robots dans la table robots et dans la table userRobots.
			bundle.put(Bundle.USER, this.users.get(i));
			bundle.put(Bundle.ROBOT,this.robots.get(i));
			this.gestionRobots.supprimerRobot(bundle);
			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
			
		}
	}
	
	//Utiliser supprimerRobots ou viderRobots dans les tests, les deux se confrontent.
	
//	@Author(auteur="Jamar")
//	@Version(version=1)
//	@Test(dependsOnMethods = "testListerRobotsUser")
//	public void testViderRobots(){
//		for(int i=0; i<4; i++){
//			bundle.put(Bundle.USER, this.users.get(i));
//			this.gestionRobots.viderRobots(bundle);
//			assertTrue((Boolean) bundle.get(Bundle.OPERATION_REUSSIE));
//		}
//	}

}
