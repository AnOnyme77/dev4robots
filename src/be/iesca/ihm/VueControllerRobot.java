package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;

@Author(auteur = "Jamar & Evrard")
@Version(version = 1)
public class VueControllerRobot extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private Model model;
	private GestionnaireUseCases gestionnaire;
	private JButton jbAjouter;
	private JButton jbSupprimer;

	// Variables nécessaires pour la JTable
	private DefaultTableModel mTable;
	private JTable jtRobots;
	private Object[][] listeRobots;

	@Author(auteur = "Jamar & Evrard")
	@Version(version = 2)
	public VueControllerRobot(Model model) {
		this.gestionnaire = GestionnaireUseCases.getInstance();
		if (model != null)
			this.model = model;

		this.setLayout(new BorderLayout());
		this.setOpaque(false);

		// Sous-titre Robots
		this.add((JLabel) StyleComp.style(StyleComp.JLABSOUSTITRE,
				"<html><u>Mes Robots</u></html>"), BorderLayout.NORTH);

		// les boutons et l'image
		JPanel jpWestGridL = new JPanel(new BorderLayout());
		jpWestGridL.setOpaque(false);

		this.jbAjouter = (JButton) StyleComp.style(StyleComp.JBUTCORP,
				"Ajouter");
		this.jbAjouter.addActionListener(this);

		this.jbSupprimer = (JButton) StyleComp.style(StyleComp.JBUTCORP,
				"Supprimer");
		this.jbSupprimer.addActionListener(this);

		JPanel jPbutAjout = new JPanel();
		jPbutAjout.add(this.jbAjouter);
		jPbutAjout.setOpaque(false);
		jpWestGridL.add(jPbutAjout, BorderLayout.NORTH);

		JPanel jPbutSupp = new JPanel();
		jPbutSupp.add(this.jbSupprimer);
		jPbutSupp.setOpaque(false);
		jpWestGridL.add(jPbutSupp);
		this.add(jpWestGridL, BorderLayout.WEST);

		// JTable
		Bundle bundle = this.model.getBundle();
		bundle.put(Bundle.USER, this.gestionnaire.getUser());
		this.gestionnaire.listerRobotUsers(bundle);
		@SuppressWarnings("unchecked")
		ArrayList<ClasseRobot> listeRobot = (ArrayList<ClasseRobot>) bundle
				.get(Bundle.LISTE);
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) {
			this.listeRobots = new Object[listeRobot.size()][1];
			int i = 0;
			for (ClasseRobot robot : listeRobot) {
				this.listeRobots[i][0] = robot;
				i++;
			}
		} else {
			this.listeRobots = new Object[0][0];
		}
		// this.model.setBundle(bundle);
		Object[] titre = { "Liste de vos robots :" };
		// Définition du TableModel par défaut
		this.mTable = new DefaultTableModel(this.listeRobots, titre);
		this.jtRobots = new JTable(this.mTable);
		LineBorder bCell = new LineBorder(Color.BLACK);
		this.jtRobots.setBorder(bCell);
		this.jtRobots.setOpaque(false);

		this.jtRobots.setIntercellSpacing(new Dimension(0, 0));// Get rid of
																// cell spacing
		this.jtRobots.setRowHeight(40);

		// Notre propre Renderer.
		CustomRenderer cr = new CustomRenderer(
				this.jtRobots.getDefaultRenderer(Object.class), Color.BLACK,
				Color.BLACK, Color.BLACK, Color.BLACK);
		this.jtRobots.setDefaultRenderer(Object.class, cr);

		JScrollPane jsp = new JScrollPane(this.jtRobots);
		jsp.getViewport().setOpaque(false);
		jsp.setOpaque(false);
		jsp.setPreferredSize(new Dimension(500, 350));
		this.add(jsp);
	}

	// Custom renderer - faites ce que le renderer doit faire naturellement,
	// ajout de bordure et de couleurs ou encore de police d'écriture perso
	public static class CustomRenderer implements TableCellRenderer {
		TableCellRenderer render;
		Border b;

		public CustomRenderer(TableCellRenderer r, Color top, Color left,
				Color bottom, Color right) {
			render = r;

			// It looks funky to have a different color on each side - but this
			// is what you asked
			// You can comment out borders if you want too. (example try
			// commenting out top and left borders)
			b = BorderFactory.createCompoundBorder();
			b = BorderFactory.createCompoundBorder(b,
					BorderFactory.createMatteBorder(0, 2, 0, 0, left));
			b = BorderFactory.createCompoundBorder(b,
					BorderFactory.createMatteBorder(0, 0, 2, 0, bottom));
			b = BorderFactory.createCompoundBorder(b,
					BorderFactory.createMatteBorder(0, 0, 0, 2, right));
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			JComponent result = (JComponent) render
					.getTableCellRendererComponent(table, value, isSelected,
							hasFocus, row, column);
			result.setBorder(b);
			result.setFont(new Font(StyleComp.JTEXTFCORP, Font.BOLD, 16));
			return result;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (this.model == null)
			return;
		if (e.getSource() == this.jbSupprimer) {
			supprimer();
		}
		if (e.getSource() == this.jbAjouter) {
			ajouter();
		}

	}

	@Author(auteur = "Jamar & Evrard", reviseur = { "Evrard" })
	@Version(version = 1)
	private void supprimer() {

		if (this.jtRobots.getSelectedRow() < 0
				|| this.jtRobots.getSelectedColumn() < 0) {
			// System.out.println("Aucun robot selectionné");
			this.model.getBundle().put(Bundle.MESSAGE,
					"Aucun robot selectionné");
			this.model.getBundle().put(Bundle.OPERATION_REUSSIE, false);
		} else {
			ClasseRobot selectionne = (ClasseRobot) this.jtRobots.getValueAt(
					this.jtRobots.getSelectedRow(),
					this.jtRobots.getSelectedColumn());
			this.model.getBundle().put(Bundle.ROBOT, selectionne);
			if (JOptionPane.showConfirmDialog(
					this,
					"Etes vous certain de vouloir supprimer "
							+ selectionne.getNom()) == JOptionPane.OK_OPTION)
				this.gestionnaire.supprimerRobot(this.model.getBundle());
			else {
				this.model.getBundle().put(Bundle.MESSAGE,
						"Suppression annulée");
				this.model.getBundle().put(Bundle.OPERATION_REUSSIE, true);
			}
		}
		this.model.setBundle(this.model.getBundle());
	}

	/*
	 * v2 -> ajout d'un message pour faire passianté
	 */
	@Author(auteur = "Evrard", reviseur = { "Hainaut" })
	@Version(version = 2)
	private void ajouter() {
		JFileChooser chooser = new JFileChooser();

		Bundle bundle = this.model.getBundle();
		bundle.put(Bundle.OPERATION_REUSSIE, true);
		bundle.put(Bundle.MESSAGE,
				"Traitement en cours, cela peut prendre quelque minute");
		this.model.setBundle(bundle);
		chooser.setMultiSelectionEnabled(false);
		chooser.setCurrentDirectory(new File("./robots/robots"));
		chooser.setFileFilter(new FileFilter() {

			@Override
			public boolean accept(File f) {
				boolean retour = false;
				if (f.getName().endsWith(".java") || f.isDirectory())
					retour = true;
				return retour;
			}

			@Override
			public String getDescription() {
				return "Java files";
			}

		});
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			File fichier = chooser.getSelectedFile();

			String nom = fichier.getName().replace(".java", "");
			try {
				ClasseRobot robot = new ClasseRobot(nom, fichier.getPath()
						.replace(fichier.getName(), ""));
				bundle.put(Bundle.ROBOT, robot);
				this.gestionnaire.ajouterRobot(bundle);
			} catch (Exception e) {
				bundle.put(Bundle.OPERATION_REUSSIE, false);
				bundle.put(Bundle.MESSAGE, e.getMessage());
				e.printStackTrace();
			}
			this.model.setBundle(bundle);
		}
	}

}
