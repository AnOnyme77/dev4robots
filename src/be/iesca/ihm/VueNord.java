package be.iesca.ihm;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;

/*
 * affichage du titre + image + pseudo + bouton de déconnection
 */
@Author( auteur="Hainaut")
@Version(version = 2)
public class VueNord extends JPanel implements ActionListener, ChangeListener{

	private static final long serialVersionUID = 1L;
	private Model model;
	private Bundle bundle;
	private GestionnaireUseCases gestionnaire;
	private JLabel jlUserNom;
	private JButton jbDeco;
	

	@Author( auteur="Hainaut")
	@Version(version = 1)
	public VueNord(Model model) {

		this.gestionnaire = GestionnaireUseCases.getInstance();
		
		if (model!=null) {
			this.model = model;
			this.model.addChangeListener(this);
		}

		this.bundle = new Bundle();

		this.setPreferredSize(new Dimension(1000, 65));
		this.setOpaque(false);
		Affichage();
	}
	
	/*
	 * v2 -> affichage correct (Hainaut)
	 * v3 -> petit oubli (Hainaut)
	 */
	@Author( auteur="Lammens & Hainaut", reviseur={"Hainaut"})
	@Version(version = 3)
	public void Affichage()
	{
		this.removeAll();
		JLabel jlabTitre = (JLabel)StyleComp.style(StyleComp.JLABTITRE);

		User user = this.gestionnaire.getUser();
		if(user!=null){
			this.setLayout(new GridLayout(0 , 3));
			
			JPanel jpGauche = new JPanel();
			jpGauche.setLayout(new FlowLayout(FlowLayout.LEFT));
			jpGauche.setOpaque(false);
			
			this.jlUserNom = (JLabel) StyleComp.style(StyleComp.JLABNOMUSER);
			jpGauche.add(this.jlUserNom);
			this.add(jpGauche);
			
			JPanel jpCentre = new JPanel();
			jpCentre.setLayout(new FlowLayout(FlowLayout.CENTER));
			jpCentre.setOpaque(false);
			
			jpCentre.add(jlabTitre);
			this.add(jpCentre);
			
			JPanel jpDroite = new JPanel();
			jpDroite.setLayout(new FlowLayout(FlowLayout.RIGHT));
			jpDroite.setOpaque(false);
			
			this.jbDeco = (JButton) StyleComp.style(StyleComp.JBUTDECO);
			this.jbDeco.addActionListener(this);
			jpDroite.add(this.jbDeco);
			this.add(jpDroite);
		}
		else{
			this.setLayout(new FlowLayout());
			this.add(jlabTitre);
		}

		this.repaint();
		this.revalidate();
	}

	/*
	 * v2 -> ajout de la méthode de déconnection
	 */
	@Author( auteur="Lammens", reviseur={"Hainaut"})
	@Version(version = 2)
	@Override
	public void actionPerformed(ActionEvent e) {
		if ( this.model == null ) return; // si pas de modèle alors ne rien faire
		if ( e.getSource() == this.jbDeco ) {
			if(JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment vous déconnecter ?")==JOptionPane.OK_OPTION){
				this.model.ClearFrames();
				this.gestionnaire.deconnecterUser(bundle);
				this.model.setBundle(bundle);
			}
	    }
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public void stateChanged(ChangeEvent e) {
		Affichage();
	}
	
	
}
