package be.iesca.ihm;
/**
 *  Vue - Contr��leur principal
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.util.Author;
import be.iesca.util.Version;

/*
 * v1 -> affichage de l'��l��ment central
 * v2 -> affiche tout (Hainaut)
 */
@Author( auteur="Lammens & Hainaut")
@Version(version = 2)
@SuppressWarnings("serial")
public class VueControllerPrincipal extends JPanel implements ChangeListener {
//	private JFrame jframe;
	
	private JPanel jpPrincipal;
	private ImageIcon icFond= new ImageIcon("Image/login.jpg");
	private Model model;
	
	/*
	 * suppretion du gestionnaire de use case
	 */
	@Author( auteur="Hainaut")
	@Version(version = 2)
	public VueControllerPrincipal(Model model, JFrame jframe) {
		
//		this.jframe=jframe;
		this.setLayout(new BorderLayout());
		

		VueNord vueTitre = new VueNord(model);
		VueSud vueSud = new VueSud(model);

		this.add(vueTitre, BorderLayout.NORTH);
		this.add(vueSud, BorderLayout.SOUTH);
		
		this.jpPrincipal = new JPanel(new FlowLayout(FlowLayout.LEFT));
		this.setPreferredSize(new Dimension(1000, 600));
		this.jpPrincipal.setOpaque(false);
		this.add(this.jpPrincipal);
		// ajouts des panels

		if (model != null) {
			this.model = model;
			this.model.addChangeListener(this);
			this.model.addJFrame(Model.PCONNEXION);
		}
		majAffichage();
		
		
	}

	/*
	 * v2 -> si on a déco revenir sur la page de connexon
	 * v3 -> changer de méthode pour le background pour éviter des conflits
	 */
	@Author( auteur="Hainaut")
	@Version(version = 3)
	private void majAffichage() {
		if (model == null)
			return;
		if(this.model.getLastFrames() == null){
			this.model.addJFrame(Model.PCONNEXION);
		}
		this.jpPrincipal.removeAll();
		switch (this.model.getLastFrames()) {
		case Model.PCONNEXION: this.jpPrincipal.add(new ControllerConnexion(this.model));
								break;
		case Model.PINSCRIPTION: this.jpPrincipal.add(new ControllerInscription(this.model));
				break;
		case Model.PMENU: this.jpPrincipal.add(new Menu(this.model));
				break;
		case Model.PBATAILLE: this.jpPrincipal.add(new VueControllerBataille(this.model));
				break;
		case Model.PROBOT: this.jpPrincipal.add(new VueControllerRobot(this.model));
				break;
		case Model.PPROFIL: this.jpPrincipal.add(new VueControllerUser(this.model));
				break;

		default:
			break;
		}
		
		;
		if(this.model.getLastFrames()!=Model.PCONNEXION && this.model.getLastFrames()!=Model.PINSCRIPTION){
			this.icFond= new ImageIcon("Image/menu.jpg");
		}
		else{
			this.icFond= new ImageIcon("Image/login.jpg");
		}
		this.jpPrincipal.repaint();
		this.jpPrincipal.revalidate();
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		majAffichage();
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		// background
		String author = "Dev4u Team - All rights reserved (c)";
		Image bg = icFond.getImage();
		int w = getWidth();
		int h = getHeight();
		g.drawImage(bg, 0, 0, w, h, Color.WHITE, null);
		g.setColor(Color.GRAY);
		g.drawString(author, 10, h-10);

	}


}
