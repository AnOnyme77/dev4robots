package be.iesca.ihm;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Programme de gestion de robots
 */
public class PgmGestionRobots {
	public static void main(String[] args) {
		/*	Swing n'est pas une API "thread safe" et ne doit ���tre manipul���e
		 *  que depuis un seul et unique thread, l'EDT.(Event Dispatch Thread)
		 *  Ceci permet d'���viter les "blocages" de l'application. 
		 *  http://gfx.developpez.com/tutoriel/java/swing/swing-threading/
		 */
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				
				//Couleurs pour Mac Os X :
				try{
				UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e1) {}
				
				FenetrePrincipale fp = new FenetrePrincipale();
				fp.setVisible(true);
			}
		});
	}
}
