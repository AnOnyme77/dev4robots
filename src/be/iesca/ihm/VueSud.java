package be.iesca.ihm;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.domaine.Bundle;
import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;


/*
 * pour le pied de page (bouton retour + partie message).
 * v1 : affichage(Hainaut)
 * v2 : ajout des actions (Lammens)
 * v3 : correction d'erreur (Hainaut)
 */
@Author( auteur="Hainaut", reviseur={"Lammens", "Hainaut"})
@Version(version = 3)
public class VueSud extends JPanel implements ChangeListener, ActionListener{

	private static final long serialVersionUID = 1L;
	private Model model;
	private JButton jbRetour;
	private JPanel jpbutRetour;
	
	@Author( auteur="Hainaut", reviseur={"Lammens", "Hainaut"})
	@Version(version = 3)
	public VueSud(Model model) {
		if (model!=null) {
			this.model = model;
			this.model.addChangeListener(this);
		}
		this.setPreferredSize(new Dimension(1000, 90));
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setOpaque(false);
		this.jpbutRetour = new JPanel();
		this.jpbutRetour.setOpaque(false);
		Affichage();
		this.add(new VueMessage(this.model));
	}

	@Author( auteur="Hainaut", reviseur={"Lammens"})
	@Version(version = 2)
	@Override
	public void stateChanged(ChangeEvent e) {
		Affichage();
	}

	@Author( auteur="Hainaut")
	@Version(version = 2)
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if ( this.model == null ) return; // si pas de modèle alors ne rien faire
		if ( e.getSource() == this.jbRetour ) {
			this.model.removeJFrame();
			this.model.getBundle().put(Bundle.MESSAGE, "");
			this.model.setBundle(this.model.getBundle());
	    }
	}

	@Author( auteur="Lammens", reviseur={"Hainaut"})
	@Version(version = 3)
	public void Affichage()
	{
		this.jpbutRetour.removeAll();
		if(model.getLastFrames()!=Model.PMENU && model.getLastFrames()!=Model.PCONNEXION)
		{
			this.jbRetour = (JButton)StyleComp.style(StyleComp.JBUTRETOUR);
			jbRetour.addActionListener(this);
			this.jpbutRetour.add(this.jbRetour);
			this.add(this.jpbutRetour, 0);
		}
		this.repaint();
		this.revalidate();
	}
	
}
