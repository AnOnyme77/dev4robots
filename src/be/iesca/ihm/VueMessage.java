package be.iesca.ihm;
/**
 *  Vue permettant l'affichage d'un message
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.domaine.Bundle;
import be.iesca.util.Author;
import be.iesca.util.Version;


@SuppressWarnings("serial")
public class VueMessage extends JLabel implements ChangeListener , ActionListener{
	private Model model;
	private Timer timer= null;

	/*
	 * v2 -> ajout d'un timer
	 */
	@Author( auteur="Legrand", reviseur={"Hainaut"})
	@Version(version = 2)
	public VueMessage(Model model) {
		this.setBorder(new TitledBorder("Message"));
		this.setPreferredSize(new Dimension(500, 60));
		if (model!=null) {
			this.model = model;
			this.model.addChangeListener(this);
			majMessage();
		}

	    this.timer = new Timer(5000, this);
	}

	private void majMessage() {
		if (model == null) return;
		Bundle bundle = this.model.getBundle();
		String message = (String) bundle.get(Bundle.MESSAGE);
		this.setText(message);
		boolean operationReussie = (Boolean) bundle
				.get(Bundle.OPERATION_REUSSIE);
		Color couleur = operationReussie ? Color.GREEN : Color.RED;
		this.setForeground(couleur);
	}

	/*
	 * v2 -> ajout d'un timer
	 */
	@Author( auteur="Legrand", reviseur={"Hainaut"})
	@Version(version = 2)
	@Override
	public void stateChanged(ChangeEvent e) {
		this.timer.stop();
		majMessage();
		this.timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.model.getBundle().put(Bundle.MESSAGE, "");
		this.timer.stop();
		majMessage();
		
	}
}
