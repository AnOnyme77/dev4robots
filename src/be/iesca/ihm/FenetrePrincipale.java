package be.iesca.ihm;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import be.iesca.util.Author;
import be.iesca.util.Version;

/**
 * Fenêtre principale de l'application
 */

@Author( auteur="Hainaut")
@Version(version = 1)
@SuppressWarnings("serial")
public class FenetrePrincipale extends JFrame {
	public FenetrePrincipale() {
		super("Ges4Robot");
		Model model = new Model("Connectez vous ou inscrivez vous.");
		VueControllerPrincipal vueControleur = new VueControllerPrincipal(model, this);
		
		this.add(vueControleur, BorderLayout.CENTER);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setLocationRelativeTo(null);
	}
}
