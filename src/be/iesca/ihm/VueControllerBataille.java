package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bataille;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;

public class VueControllerBataille extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private Model model;
	private GestionnaireUseCases gestionnaire;
	
	private JButton jbNouveau;
	private JButton jbSupprimer;
	private JButton jbModifier;
	private JButton jbEnregistrer;
	private JButton jbLancer;
	
	private JTextField jtfNom;
	private JLabel jlNom;
	
	//Variables nécessaires pour la JTable
	private DefaultTableModel mTable;
	private JTable jtBatailles;
	private JTable jtRobots;
	private Object[][] listeBatailles;
	private Object[][] listeRobots;

	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	public VueControllerBataille(Model model) {
		this.gestionnaire = GestionnaireUseCases.getInstance();
		if (model != null)
			this.model = model;
		this.setLayout(new BorderLayout(5,5));
		this.setOpaque(false);

		/*
		 * 
		 * Partie NORD 
		 * 
		 */
		// Sous-titre Batailles
		this.add((JLabel) StyleComp.style(StyleComp.JLABSOUSTITRE,
				"<html><u>Mes Batailles</u></html>"), BorderLayout.NORTH);

		/*
		 * 
		 * Partie OUEST 
		 * 
		 */
		// les boutons et l'image
		JPanel jpWestGridL = new JPanel(new GridLayout(5, 1, 0, 0));
		jpWestGridL.setOpaque(false);
		
		this.jbNouveau = (JButton) StyleComp.style(StyleComp.JBUTCORP, "Nouveau");
		this.jbNouveau.addActionListener(this);

		this.jbSupprimer = (JButton) StyleComp.style(StyleComp.JBUTCORP, "Supprimer");
		this.jbSupprimer.addActionListener(this);
		
		this.jbModifier = (JButton) StyleComp.style(StyleComp.JBUTCORP, "Détails");
		this.jbModifier.addActionListener(this);
		
		this.jbEnregistrer = (JButton) StyleComp.style(StyleComp.JBUTCORP, "Enregistrer");
		this.jbEnregistrer.addActionListener(this);
		
		this.jbLancer = (JButton) StyleComp.style(StyleComp.JBUTCORP, "Lancer Bataille !");
		this.jbLancer.addActionListener(this);

		JPanel jPbutNouveau = new JPanel();
		jPbutNouveau.add(this.jbNouveau);
		jPbutNouveau.setOpaque(false);
		jpWestGridL.add(jPbutNouveau);

		JPanel jPbutSupprimer = new JPanel();
		jPbutSupprimer.add(this.jbSupprimer);
		jPbutSupprimer.setOpaque(false);
		jpWestGridL.add(jPbutSupprimer);
		
		JPanel jPbutModifier = new JPanel();
		jPbutModifier.add(this.jbModifier);
		jPbutModifier.setOpaque(false);
		jpWestGridL.add(jPbutModifier);
		
		JPanel jPbutEnregistrer = new JPanel();
		jPbutEnregistrer.add(this.jbEnregistrer);
		jPbutEnregistrer.setOpaque(false);
		jpWestGridL.add(jPbutEnregistrer);
		
		JPanel jPbutLancer = new JPanel();
		jPbutLancer.add(this.jbLancer);
		jPbutLancer.setOpaque(false);
		jpWestGridL.add(jPbutLancer);
		
		this.add(jpWestGridL, BorderLayout.WEST);

		/*
		 * 
		 * Partie Centrale 
		 * 
		 */
		
		// JTable
		Bundle bundle = this.model.getBundle();
		User monuser = this.gestionnaire.getUser();
		bundle.put(Bundle.USER, monuser);
		Bataille mabataille = new Bataille();
		mabataille.setEmail(monuser.getEmail());
		bundle.put(Bundle.BATAILLE,  mabataille);
		this.gestionnaire.listerBataille(bundle);
		
		@SuppressWarnings("unchecked")
		ArrayList<Bataille> listeBataille = (ArrayList<Bataille>) bundle.get(Bundle.LISTE);
		if((boolean)bundle.get(Bundle.OPERATION_REUSSIE)){
			this.listeBatailles = new Object[listeBataille.size()][1];
			int i=0;
			for(Bataille bataille : listeBataille){
				this.listeBatailles[i][0]=bataille;
				i++;
			}
		}
		else{
			this.listeBatailles = new Object[0][0];
		}
		Object[] titre = { "Liste de vos batailles :" };

		//Définition du TableModel par défaut
		this.mTable = new DefaultTableModel(this.listeBatailles, titre);
		this.jtBatailles = new JTable(this.mTable);
		LineBorder bCell = new LineBorder(Color.BLACK);
		this.jtBatailles.setBorder(bCell);
		this.jtBatailles.setOpaque(false);
		
		this.jtBatailles.setIntercellSpacing(new Dimension(0,0));//Get rid of cell spacing
		this.jtBatailles.setRowHeight(40);
		
		//Notre propre Renderer.
        CustomRenderer cr = new CustomRenderer(this.jtBatailles.getDefaultRenderer(Object.class), Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);
        this.jtBatailles.setDefaultRenderer(Object.class, cr);
        
        JScrollPane jsp = new JScrollPane(this.jtBatailles);
        jsp.getViewport().setOpaque(false);
        jsp.setOpaque(false);
        jsp.setPreferredSize(new Dimension(300,100));
		this.add(jsp, BorderLayout.CENTER);
		
		/*
		 * 
		 * Partie EST 
		 * 
		 */
		
		JPanel jpWest = new JPanel();
		jpWest.setLayout(new BorderLayout());
		jpWest.setOpaque(false);
		
		// creation du jlabel et du jtextfield pour le nom de la bataille
		
		this.jlNom = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Nom de la bataille :");
		this.jtfNom = (JTextField)StyleComp.style(StyleComp.JTEXTFCORP);
		
		JPanel jpWestTop = new JPanel();
		jpWestTop.setOpaque(false);
		jpWestTop.add(this.jlNom);
		jpWestTop.add(this.jtfNom);
		
		jpWest.add(jpWestTop ,BorderLayout.NORTH);
		
		// creation de la jtable avec la liste des robots
		this.gestionnaire.listerRobots(bundle);
		@SuppressWarnings("unchecked")
		ArrayList<ClasseRobot> listeRobot = (ArrayList<ClasseRobot>) bundle.get(Bundle.LISTE);
		if((boolean)bundle.get(Bundle.OPERATION_REUSSIE)){
			this.listeRobots = new Object[listeRobot.size()][2];
			int i=0;
			for(ClasseRobot robot : listeRobot){
				this.listeRobots[i][0]=robot.getNom();
				this.listeRobots[i][1]=false;
				i++;
			}
		}
		else{
			this.listeRobots = new Object[0][0];
		}
		Object[] titreRobot = { "Liste de vos robots :","Selection" };

		//Définition du TableModel par défaut
		this.mTable = new TableSelectRobots(this.listeRobots, titreRobot);
		this.jtRobots = new JTable(this.mTable);
		this.jtRobots.setBorder(bCell);
		this.jtRobots.setOpaque(false);
		
		this.jtRobots.setIntercellSpacing(new Dimension(0,0));
		this.jtRobots.setRowHeight(40);
		
		// modification de la taille de la 2eme colonne
		
		this.jtRobots.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		TableColumn col = this.jtRobots.getColumnModel().getColumn(1);
	    col.setPreferredWidth(70);
	    
	    col = this.jtRobots.getColumnModel().getColumn(0);
	    col.setPreferredWidth(398);
		
		//Notre propre Renderer.
        cr = new CustomRenderer(this.jtRobots.getDefaultRenderer(Object.class), Color.BLACK, Color.BLACK, Color.BLACK, Color.BLACK);
        this.jtRobots.setDefaultRenderer(Object.class, cr);
        
        jsp = new JScrollPane(this.jtRobots,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jsp.getViewport().setOpaque(false);
        jsp.setOpaque(false);
        jsp.setPreferredSize(new Dimension(470,300));
		jpWest.add(jsp, BorderLayout.CENTER);
		
		this.add(jpWest, BorderLayout.EAST);
		
		
	}

	//Custom renderer - faites ce que le renderer doit faire naturellement,
	// ajout de bordure et de couleurs ou encore de police d'écriture perso
	@Author(auteur="Evrard & Jamar")
	@Version(version=1)
	public static class CustomRenderer implements TableCellRenderer{
        TableCellRenderer render;
        Border b;
        public CustomRenderer(TableCellRenderer r, Color top, Color left,Color bottom, Color right){
            render = r;

            //It looks funky to have a different color on each side - but this is what you asked
            //You can comment out borders if you want too. (example try commenting out top and left borders)
            b = BorderFactory.createCompoundBorder();
            b = BorderFactory.createCompoundBorder(b, BorderFactory.createMatteBorder(0,2,0,0,left));
            b = BorderFactory.createCompoundBorder(b, BorderFactory.createMatteBorder(0,0,2,0,bottom));
            b = BorderFactory.createCompoundBorder(b, BorderFactory.createMatteBorder(0,0,0,2,right));
        }
        @Author(auteur="Evrard & Jamar")
    		@Version(version=1)
        public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {
            JComponent result = (JComponent)render.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            result.setBorder(b);
            result.setFont(new Font(StyleComp.JTEXTFCORP, Font.BOLD, 16));
            return result;
        }
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		if ( this.model == null ) return;
		if ( e.getSource() == this.jbSupprimer ) 
		{
        	supprimer();
		}
		else if(e.getSource()==this.jbNouveau)
		{
			nouveau();
		}
		else if(e.getSource() == this.jbModifier)
		{
			modifier();
		}
		else if(e.getSource() == this.jbLancer)
		{
			lancer();
		}
		else if(e.getSource() == this.jbEnregistrer)
		{
			enregistrer();
		}

	}
	
	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	private void supprimer() {

		Bundle bundle = this.model.getBundle();
		if(this.jtBatailles.getSelectedRow() < 0 || this.jtBatailles.getSelectedColumn() < 0)
		{
			this.model.getBundle().put(Bundle.MESSAGE, "Aucune bataille selectionnée");
			this.model.getBundle().put(Bundle.OPERATION_REUSSIE, false);
		}
		else{
			Bataille selectionne =  (Bataille)this.jtBatailles.getValueAt(this.jtBatailles.getSelectedRow(), 0);
			bundle.put(Bundle.BATAILLE, selectionne);
			if(JOptionPane.showConfirmDialog(this, "Etes vous certain de vouloir supprimer "+selectionne.getNom())==JOptionPane.OK_OPTION){
				bundle.put(Bundle.BATAILLE, selectionne);
				this.gestionnaire.supprimerBataille(bundle);
				this.model.setBundle(bundle);
			}
			else{
				this.model.getBundle().put(Bundle.MESSAGE, "Suppression annulée");
				this.model.getBundle().put(Bundle.OPERATION_REUSSIE, true);
			}
		}
		
	}
	
	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	private void clearForNewBataille(){
		jtfNom.setEnabled(true);
		jtfNom.setText("");
		for(int i=0; i<this.jtRobots.getRowCount(); i++)
				this.jtRobots.setValueAt(false, i, 1);
		
	}
	
	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	private void nouveau() {
		if(!jtfNom.getText().isEmpty()){
			if(JOptionPane.showConfirmDialog(this, "Etes vous certain de vouloir abandonner les modifications en cours ? ")==JOptionPane.OK_OPTION)
				clearForNewBataille();
		}
		else
			clearForNewBataille();
	}
	
	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	private void modifier() {
		Bundle bundle = this.model.getBundle();
		if(this.jtBatailles.getSelectedRow() < 0 || this.jtBatailles.getSelectedColumn() < 0)
		{
			this.model.getBundle().put(Bundle.MESSAGE, "Aucune bataille selectionnée");
			this.model.getBundle().put(Bundle.OPERATION_REUSSIE, false);
		}
		else{
			Bataille selectionne =  (Bataille)this.jtBatailles.getValueAt(this.jtBatailles.getSelectedRow(), 0);
			bundle.put(Bundle.BATAILLE, selectionne);
			this.gestionnaire.recupererBataille(bundle);
			selectionne = (Bataille) bundle.get(Bundle.BATAILLE);
			/*Afficher la bataille dans le menu à droite pour l'édition*/
			jtfNom.setText(selectionne.getNom());
			jtfNom.setEnabled(false);
			String[]listeRobotsBataille = selectionne.listeRobots();
			ArrayList<String> arrayListeRobots = new ArrayList<String>();
			
			for(int i=0; i<listeRobotsBataille.length; i++){
				arrayListeRobots.add(listeRobotsBataille[i]);
			}
			for(int i=0; i<this.jtRobots.getRowCount(); i++){
				if(arrayListeRobots.contains(this.jtRobots.getValueAt(i, 0))){
					this.jtRobots.setValueAt(true, i, 1);
				}
				else
					this.jtRobots.setValueAt(false, i, 1);
			}
			
		}
	}
	
	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	private void lancer() {
		Bundle bundle = new Bundle();
		Bataille selectionne =  (Bataille)this.jtBatailles.getValueAt(this.jtBatailles.getSelectedRow(), 0);
		bundle.put(Bundle.BATAILLE, selectionne);
		this.gestionnaire.recupererBataille(bundle);
		selectionne = (Bataille) bundle.get(Bundle.BATAILLE);
		this.gestionnaire.lancerBataille(bundle);
	}
	
	@Author(auteur="Lammens & Jamar")
	@Version(version=1)
	private void enregistrer(){
		Bundle bundle = this.model.getBundle();
		if(this.jtfNom.getText().isEmpty())
		{
			this.model.getBundle().put(Bundle.MESSAGE, "Veuillez donner un nom à la bataille");
			this.model.getBundle().put(Bundle.OPERATION_REUSSIE, false);			
		}
		else
		{
			Bataille bataille = new Bataille();
			bataille.setNom(this.jtfNom.getText());
			bataille.setEmail(this.gestionnaire.getUser().getEmail());
			
				for(int i = 0; i<this.jtRobots.getRowCount(); i++){
					if((boolean) this.jtRobots.getValueAt(i, 1))
						bataille.ajouterRobot((String)this.jtRobots.getValueAt(i, 0));
				}
				bundle.put(Bundle.BATAILLE, bataille);
				/*Faire la différence si on est dans un ajout ou dans une modification*/
				
				
				if(!jtfNom.isEnabled()){
					this.gestionnaire.modifierBataille(bundle);
				}
				else{
					this.gestionnaire.ajouterBataille(bundle);
				}
		}
			this.model.setBundle(bundle);
	}

}
