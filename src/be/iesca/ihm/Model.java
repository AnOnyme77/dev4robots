package be.iesca.ihm;
/**
 * Modèle de données (mvc1) contenant un bundle
 */
import java.util.ArrayList;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import be.iesca.domaine.Bundle;
import be.iesca.util.Author;
import be.iesca.util.Version;

/*
 * v1 -> ajout de la liste des Frames (Lammens)
 * v2 -> ajout de variable pour tjrs jouer avec les mêmes valeurs pour tout le mode (Hainaut)
 */
@Author( auteur="Lamens", reviseur={"Hainaut"})
@Version(version = 2) 
public class Model {

	public static final String PCONNEXION = "ControllerConnexion";
	public static final String PINSCRIPTION = "ControllerInscription";
	public static final String PMENU = "Menu";
	public static final String PBATAILLE = "VueControllerBataille";
	public static final String PPROFIL = "VueControllerProfil";
	public static final String PROBOT = "VueControllerRobot";
	
	private ArrayList<String> listeFrames;
	private Bundle bundle;
	private ArrayList<ChangeListener> listeVues;

	@Author( auteur="Lamens")
	@Version(version = 2) 
	public String getLastFrames() {
		if(listeFrames.size()==0)
			return null;
		return listeFrames.get(listeFrames.size() - 1);
	}

	@Author( auteur="Lamens")
	@Version(version = 1) 
	public void ClearFrames(){
		this.listeFrames = new ArrayList<String>(1);	
	}

	public Model() {
		this.bundle = new Bundle();
		this.bundle.put(Bundle.OPERATION_REUSSIE, true);
		this.listeVues = new ArrayList<ChangeListener>(1);
		this.listeFrames = new ArrayList<String>(1);	
	}

	public Model(String message) {
		this();
		this.bundle.put(Bundle.MESSAGE, message);
	}

	public void setBundle(Bundle bundle) {
		this.bundle = bundle;
	    traiterEvent(  new ChangeEvent( this ) );
	}

	/** Enregistre un listener */
	public synchronized void addChangeListener(ChangeListener chl) {
		if (!listeVues.contains(chl)) {
			listeVues.add(chl);
		}
	}

	/** supprime un listener */
	public synchronized void removeChangeListener(ChangeListener chl) {
		if (listeVues.contains(chl)) {
			listeVues.remove(chl);
		}
	}

	/** notifie le(s) listener(s) */
	protected synchronized void traiterEvent(ChangeEvent e) {
		for (ChangeListener listener : listeVues) {
			listener.stateChanged(e);
		}
	}
	
	/** Enregistre la JFrame */
	@Author( auteur="Lamens")
	@Version(version = 1) 
	public synchronized void addJFrame(String jframe) {
		if (!listeFrames.contains(jframe)) {
			listeFrames.add(jframe);
		}
		traiterEvent(new ChangeEvent(this));
	}

	/** supprime la JFrame */
	@Author( auteur="Lammens", reviseur={"Hainaut"})
	@Version(version = 2) 
	public synchronized void removeJFrame() {
		if(listeFrames.size()==0) return ;
		listeFrames.remove(listeFrames.get(listeFrames.size() - 1));
		traiterEvent(new ChangeEvent(this));
	}

	public Bundle getBundle() {
		return this.bundle;
	}

}
