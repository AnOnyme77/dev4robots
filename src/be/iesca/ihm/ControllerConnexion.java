package be.iesca.ihm;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;


@Author( auteur="Lammens")
@Version(version = 1)
public class ControllerConnexion extends JPanel implements ActionListener{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlEmail;
	private JLabel jlMDP;
	private JTextField jtfEmail;
	private JPasswordField jpfMDP;
	private JButton jbConnexion;
	private JButton jbInscription;
	private GestionnaireUseCases gestionnaire;
	private Bundle bundle;
	private Model model;
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	public ControllerConnexion(Model model) {
		super();
		this.gestionnaire = GestionnaireUseCases.getInstance();
		if (model!=null) {
			this.model = model;
		}
					
		// panel central
		this.setOpaque(false);
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		// panel centrale gauche
		JPanel jpCG = new JPanel();
		jpCG.setOpaque(false);
		jpCG.setLayout(new GridLayout(2, 1, 50, 50));
		
		JPanel jpCGN = new JPanel();
		jpCGN.setOpaque(false);
		jpCGN.setLayout(new GridLayout(2,2,10,10));
		JPanel jpCGS = new JPanel();
		jpCGS.setOpaque(false);
		jpCGS.setLayout(new GridLayout(2,1));
		
		// creation des champs
		this.jlEmail = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Email :");
		this.jlMDP = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Mot de passe :");
		this.jtfEmail = (JTextField)StyleComp.style(StyleComp.JTEXTFCORP);
		this.jpfMDP = (JPasswordField)StyleComp.style(StyleComp.JPASSFCORP);
		
		// creation des bouttons
		this.jbConnexion = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Connexion");
		jbConnexion.addActionListener(this);
		
		this.jbInscription = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Inscription");
		jbInscription.addActionListener(this);
		
		// ajout des panel pour le boutton de connexion
		JPanel jpConnexion = new JPanel();
		jpConnexion.setOpaque(false);
		jpConnexion.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpConnexion.add(jbConnexion);
		
		JPanel jpInscription = new JPanel();
		jpInscription.setOpaque(false);
		jpInscription.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpInscription.add(jbInscription);
		
		// ajout des champs
		jpCGN.add(jlEmail);
		jpCGN.add(jtfEmail);
		jpCGN.add(jlMDP);
		jpCGN.add(jpfMDP);
		
		jpCGS.add(jpConnexion);
		jpCGS.add(jpInscription);
		
		//ajout des panel
		jpCG.add(jpCGN);
		jpCG.add(jpCGS);
		this.add(jpCG);	
		
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public void actionPerformed(ActionEvent e) {
		if ( model == null ) return; // si pas de modèle alors ne rien faire

        if ( e.getSource() == jbConnexion ) 
        {
        	connecter();
	    }
        else if(e.getSource()== jbInscription)
        {
        	this.model.addJFrame(Model.PINSCRIPTION);
        	if(this.bundle == null)
        		if(this.model.getBundle()==null)
        			this.bundle = new Bundle();
        		else
        			this.bundle=this.model.getBundle();

        	this.bundle.put(Bundle.OPERATION_REUSSIE, true);
        	this.bundle.put(Bundle.MESSAGE, "Entrez les informations dans les champs");
        	this.bundle.put(Bundle.USER, null);
        	this.model.setBundle(this.bundle);
        	
        }
		
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	public void setModel ( Model model ) {
	      this.model = model;
	}
	
	/*
	 * v2 -> acces au menu quand connection ok et affichage du message dans le bon champs. (Hainaut)
	 */
	@Author( auteur="Lammens", reviseur={"Hainaut"})
	@Version(version = 2)
	protected void connecter() {
		User user = new User(this.jtfEmail.getText(), new String(this.jpfMDP.getPassword()));
		this.bundle = new Bundle();
		bundle.put(Bundle.USER, user);
		this.gestionnaire.connecterUser(bundle);
		if ((boolean) bundle.get(Bundle.OPERATION_REUSSIE)) {
			this.model.addJFrame(Model.PMENU);
		} 
		this.model.setBundle(bundle);
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	public Bundle getBundle() {
		return bundle;
	}

}
