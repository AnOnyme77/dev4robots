package be.iesca.ihm;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;

@Author( auteur="Lammens")
@Version(version = 1)
public class ControllerInscription extends JPanel implements ActionListener{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlInscription;
	private JLabel jlEmail;
	private JLabel jlPseudo;
	private JLabel jlMDP;
	private JLabel jlConfMDP;
	private JTextField jtfEmail;
	private JTextField jtfPseudo;
	private JPasswordField jpfMDP;
	private JPasswordField jpfConfMDP;
	private JButton jbEnregistrer;
	
	private GestionnaireUseCases gestionnaire;
	private Bundle bundle;
	private Model model;
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	public ControllerInscription(Model model) {
		super();
		if (model!=null) {
			this.model = model;
		}
		this.gestionnaire = GestionnaireUseCases.getInstance();
				
		// panel central
		this.setOpaque(false);
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		// panel centrale gauche
		JPanel jpCG = new JPanel();
		jpCG.setOpaque(false);
		jpCG.setLayout(new GridBagLayout());
		
		// creation des champs
		this.jlInscription = (JLabel)StyleComp.style(StyleComp.JLABSOUSTITRE,"<HTML><u>Inscription</u></HTML>");
		
		// creation des label
		this.jlEmail = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Email :");
		this.jlPseudo =(JLabel)StyleComp.style(StyleComp.JLABCORP, "Pseudo :");
		this.jlMDP = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Mot de passe :");
		this.jlConfMDP = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Confirmer Mot de passe:");
		
		// creation des zone de texte
		this.jtfEmail = (JTextField)StyleComp.style(StyleComp.JTEXTFCORP);
		this.jtfPseudo = (JTextField)StyleComp.style(StyleComp.JTEXTFCORP);
		this.jpfMDP = (JPasswordField)StyleComp.style(StyleComp.JPASSFCORP);
		this.jpfConfMDP = (JPasswordField)StyleComp.style(StyleComp.JPASSFCORP);

		// ajout des valeurs si l'utilisateur a ete ajouté
		User user = (User) this.model.getBundle().get(Bundle.USER);
		if(user!=null)
		{
			this.jtfEmail.setText(user.getEmail());
			this.jtfPseudo.setText(user.getPseudo());
		}
			
		// creation des boutons
		this.jbEnregistrer = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Enregistrer");
		jbEnregistrer.addActionListener(this);
		
		
		// ajout des panel pour les boutons
		JPanel jpEnregistrer= new JPanel();
		jpEnregistrer.setOpaque(false);
		jpEnregistrer.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpEnregistrer.add(jbEnregistrer);
	
		
		// ajout des champs
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.insets = new Insets(10,10,10,10);
		constraints.anchor = GridBagConstraints.LINE_START;
		
		constraints.gridx=0;
		constraints.gridy=0;
		jpCG.add(jlInscription, constraints);
		
		constraints.gridx=0;
		constraints.gridy=1;
		jpCG.add(jlEmail, constraints);
		constraints.gridx=1;
		constraints.gridy=1;
		jpCG.add(jtfEmail, constraints);
		constraints.gridx=0;
		constraints.gridy=2;
		jpCG.add(jlPseudo, constraints);
		constraints.gridx=1;
		constraints.gridy=2;
		jpCG.add(jtfPseudo, constraints);
		constraints.gridx=0;
		constraints.gridy=3;
		jpCG.add(jlMDP, constraints);
		constraints.gridx=1;
		constraints.gridy=3;
		jpCG.add(jpfMDP, constraints);
		constraints.gridx=0;
		constraints.gridy=4;
		jpCG.add(jlConfMDP, constraints);
		constraints.gridx=1;
		constraints.gridy=4;
		jpCG.add(jpfConfMDP, constraints);
		
		constraints.gridx=1;
		constraints.gridy=5;
		jpCG.add(jpEnregistrer, constraints);
		
		constraints.gridx=0;
		constraints.gridy=6;
		
		//ajout des panel
		this.add(jpCG);
		
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public void actionPerformed(ActionEvent e) {
		if ( model == null ) return; // si pas de modèle alors ne rien faire

        if ( e.getSource() == jbEnregistrer ) 
        {
        	ajouter();
	    }
		
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	private void ajouter() {
		String pseudo = jtfPseudo.getText();
		String Email = jtfEmail.getText();
		String mdp1 = new String(jpfMDP.getPassword());
		String mdp2 = new String(jpfConfMDP.getPassword());
				
		this.bundle = model.getBundle();
		if(mdp1.equals(mdp2))
		{
			User user = new User(Email, pseudo, mdp1);
			this.bundle.put(Bundle.USER, user);
			this.gestionnaire.ajouterUser(bundle);
		}
		else
		{
			bundle.put(Bundle.MESSAGE, "Erreur : mot de passe différent");
			bundle.put(Bundle.OPERATION_REUSSIE, false);
		}
		if((boolean) this.bundle.get(Bundle.OPERATION_REUSSIE))
		{
			this.model.removeJFrame();
			this.bundle.put(Bundle.USER, null);
		}

		this.model.setBundle(bundle);
		

	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	public void setModel ( Model model ) {
	      this.model = model;
	}

}
