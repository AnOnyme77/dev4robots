package be.iesca.ihm;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class TableSelectRobots extends DefaultTableModel{

	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public TableSelectRobots(Object[][] liste, Object[] titre) {
	      super(liste, titre);
	    }

	    @Override
	    public Class<?> getColumnClass(int columnIndex) {
	      @SuppressWarnings("rawtypes")
		Class clazz = String.class;
	      switch (columnIndex) {
	        case 0:
	          clazz = String.class;
	          break;
	        case 1:
	          clazz = Boolean.class;
	          break;
	      }
	      return clazz;
	    }

	    @Override
	    public boolean isCellEditable(int row, int column) {
	      return column == 1;
	    }

	    @SuppressWarnings({ "unchecked", "rawtypes" })
		@Override
	    public void setValueAt(Object aValue, int row, int column) {
	      if (aValue instanceof Boolean && column == 1) {
	        Vector rowData = (Vector)getDataVector().get(row);
	        rowData.set(1, (boolean)aValue);
	        fireTableCellUpdated(row, column);
	      }
	    }

	  }

