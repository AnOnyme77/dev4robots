package be.iesca.ihm;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import be.iesca.util.Author;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;

/*
 * v2 -> ajout des actions (Hainaut)
 */
@Author( auteur="Hainaut")
@Version(version = 2) 
public class Menu extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;

	private JButton jbProfil;
	private JButton jbRobot;
	private JButton jbBataille;
	private Model model;

	@Author( auteur="Hainaut")
	@Version(version = 2) 
	public Menu(Model model){
		
		if (model!=null) {
			this.model = model;
		}
		this.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 80));
		this.setOpaque(false);
		JPanel jpGridL = new JPanel(new GridLayout(0,1));
		jpGridL.setOpaque(false);
		
		this.jbRobot = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Mes Robots");
		this.jbRobot.addActionListener(this);
		this.jbBataille = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Mes Batailles");
		this.jbBataille.addActionListener(this);
		this.jbProfil = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Mon Profil");
		this.jbProfil.addActionListener(this);
		
		JPanel jPRobot = new JPanel();
		jPRobot.setOpaque(false);
		jPRobot.setPreferredSize(new Dimension(500, 100));
		jPRobot.add(this.jbRobot);
		jpGridL.add(jPRobot);
		
		JPanel jPBataille = new JPanel();
		jPBataille.setOpaque(false);
		jPBataille.setPreferredSize(new Dimension(500, 100));
		jPBataille.add(this.jbBataille);
		jpGridL.add(jPBataille);
		
		JPanel jPProfil = new JPanel();
		jPProfil.setOpaque(false);
		jPProfil.setPreferredSize(new Dimension(500, 100));
		jPProfil.add(this.jbProfil);
		jpGridL.add(jPProfil);
		
		this.add(jpGridL);
		
	}

	@Author( auteur="Hainaut")
	@Version(version = 2) 
	@Override
	public void actionPerformed(ActionEvent e) {
		if ( this.model == null ) return; // si pas de modèle alors ne rien faire
		if ( e.getSource() == this.jbRobot ) {
        	this.model.addJFrame(Model.PROBOT);
	    }else if ( e.getSource() == this.jbBataille ) {
        	this.model.addJFrame(Model.PBATAILLE);
	    }else if ( e.getSource() == this.jbProfil ) {
        	this.model.addJFrame(Model.PPROFIL);
	    }
		this.model.setBundle(this.model.getBundle());
	}
}
