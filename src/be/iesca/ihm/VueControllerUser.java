package be.iesca.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Fichier;
import be.iesca.util.StyleComp;
import be.iesca.util.Version;

/* v1 -> affichage
 * v2 -> ajout des actions
 * v3 -> ajout de la modification de l'image
 */
@Author( auteur="Hainaut")
@Version(version = 3) 
public class VueControllerUser extends JPanel implements ActionListener{


	private static final long serialVersionUID = 1L;
	private JTextField jtfPseudo;
	private JPasswordField jpfMDP;
	private JPasswordField jpfMDPConf;
	private JButton jbEnreg;
	private JButton jbChImage;
	private String imageUser;
	private JLabel jlabImage;
	private Model model;
	private GestionnaireUseCases gestionnaire;
	private String pathImage= null;

	/*
	 * v3 -> pseudo par default (Hainaut)
	 * v4 -> ajout de l'image de profil
	 */
	@Author( auteur="Hainaut")
	@Version(version = 4)
	public VueControllerUser(Model model){
		this.gestionnaire = GestionnaireUseCases.getInstance();
		if (model!=null) {
			this.model = model;
		}
		this.gestionnaire.afficherUser(this.model.getBundle());
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(1000, 350));
		this.setOpaque(false);
		
		// sous-titre compte
		this.add((JLabel)StyleComp.style(StyleComp.JLABSOUSTITRE, "<html><u>Mon Profil</u></html>") ,BorderLayout.NORTH);
		

		// les champs 
		JPanel jpCentreFlowL = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel jpCentreGridL = new JPanel(new GridLayout(0,2));
		jpCentreFlowL.setOpaque(false);
		jpCentreGridL.setOpaque(false);
		
		JLabel jlPseudo = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Pseudo :");
		this.jtfPseudo = (JTextField)StyleComp.style(StyleComp.JTEXTFCORP, ((User) this.model.getBundle().get(Bundle.USER)).getPseudo());

		JLabel jlMDP = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Nouveau Mot de passe :");
		this.jpfMDP = (JPasswordField)StyleComp.style(StyleComp.JPASSFCORP);
		
		JLabel jlMDPConf = (JLabel)StyleComp.style(StyleComp.JLABCORP, "Confirmer Mot de passe :");
		this.jpfMDPConf = (JPasswordField)StyleComp.style(StyleComp.JPASSFCORP);
				
		jpCentreGridL.add(jlPseudo);
		JPanel jPEmail = new JPanel();
		jPEmail.setOpaque(false);
		jPEmail.add(this.jtfPseudo);
		jpCentreGridL.add(jPEmail);
		
		jpCentreGridL.add(jlMDP);
		JPanel jPMDP = new JPanel();
		jPMDP.setOpaque(false);
		jPMDP.add(this.jpfMDP);
		jpCentreGridL.add(jPMDP);
		
		jpCentreGridL.add(jlMDPConf);
		JPanel jPMDPConf = new JPanel();
		jPMDPConf.setOpaque(false);
		jPMDPConf.add(this.jpfMDPConf);
		jpCentreGridL.add(jPMDPConf);
		
		jpCentreFlowL.add(jpCentreGridL);
		this.add(jpCentreFlowL, BorderLayout.CENTER);
		
		// les boutons et l'image
		JPanel jpSudGridL = new JPanel(new GridLayout(0,2));
		jpSudGridL.setOpaque(false);
		
		this.jbEnreg = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Enregistrer");
		this.jbEnreg.addActionListener(this);
		
		this.jbChImage = (JButton)StyleComp.style(StyleComp.JBUTCORP, "Changer Image");
		this.jbChImage.addActionListener(this);


		JPanel jpimage = new JPanel();
		jpimage.setOpaque(false);
		this.imageUser = ((User) this.model.getBundle().get(Bundle.USER)).getImage();
		this.jlabImage= new JLabel( new ImageIcon(new ImageIcon(this.imageUser).getImage().getScaledInstance(70, 70, Image.SCALE_DEFAULT)));
		this.jlabImage.setOpaque(false);
		this.jlabImage.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, StyleComp.NOIR));
		jpimage.add(this.jbChImage);
		jpimage.add(this.jlabImage);
		
		JPanel jPbutEnreg = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 17));
		jPbutEnreg.add(this.jbEnreg);
		jPbutEnreg.setOpaque(false);
		jpSudGridL.add(jPbutEnreg);
		
		jpSudGridL.add(jpimage);
		
		this.add(jpSudGridL, BorderLayout.SOUTH);
	}

	@Author( auteur="Hainaut")
	@Version(version = 1)
	@Override
	public void actionPerformed(ActionEvent e) {
		// si on veut enregistrer (ou si on veut changer Image)
		if ( this.model == null ) return; // si pas de modeele alors ne rien faire
		if ( e.getSource() == this.jbEnreg ) {
        	enregistrer();
	    }
		if ( e.getSource() == this.jbChImage ) {
			modifierImage();
	    }

		this.repaint();
		this.revalidate();
	}

	@Author( auteur="Hainaut")
	@Version(version = 1)
	private void modifierImage() {
		JFileChooser chooser = new JFileChooser();
		chooser.setMultiSelectionEnabled(false);
		
		chooser.setFileFilter(new FileFilter(){

			@Override
			public boolean accept(File f) {
				boolean retour=false;
				if(f.getName().endsWith(".png")||f.getName().endsWith(".jpg")||f.getName().endsWith(".jpeg")||f.isDirectory())
					retour=true;
				return retour;
			}

			@Override
			public String getDescription() {
				return "Java files";
			}
			
		});
		chooser.setDialogTitle("choisir Image de profil");
		chooser.setCurrentDirectory(new File("./ImageProfil"));
		int option = chooser.showOpenDialog( this );
		if (option == JFileChooser.APPROVE_OPTION) {
			File fichier = chooser.getSelectedFile();
			
			String nom = fichier.getName();
			try {
				this.imageUser=Bundle.REPERTOIRE_IMAGE+nom;
				this.pathImage= fichier.getPath().replace(fichier.getName(), "");
				this.jlabImage.setIcon(new ImageIcon(new ImageIcon(fichier.getPath()).getImage().getScaledInstance(70, 70, Image.SCALE_DEFAULT)));
				this.repaint();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/*
	 * v1 -> enregistrement de tout sauf image
	 * v2 -> + image
	 * v3 -> pas de lancement d'exception si pas de fichier sélèctionner
	 */
	@Author( auteur="Hainaut")
	@Version(version = 3)
	private void enregistrer() {
		String pseudo = this.jtfPseudo.getText();
		String mdp1 = new String(this.jpfMDP.getPassword());
		String mdp2 = new String(this.jpfMDPConf.getPassword());
		this.gestionnaire.afficherUser(this.model.getBundle());
		if((boolean)this.model.getBundle().get(Bundle.OPERATION_REUSSIE)){
			User user = (User)this.model.getBundle().get(Bundle.USER);
			if(mdp1.equals(mdp2))
			{
				if(mdp1!=null && !mdp1.isEmpty()) user.setPassword(mdp1);
				if(pseudo != null && !pseudo.isEmpty()) 
				{
					user.setPseudo(pseudo);
					if(this.pathImage!=null){
						Fichier.copierFichier(this.imageUser.replace(Bundle.REPERTOIRE_IMAGE, ""), this.pathImage, Bundle.REPERTOIRE_IMAGE);
						this.jlabImage.setIcon(new ImageIcon(new ImageIcon(this.imageUser).getImage().getScaledInstance(70, 70, Image.SCALE_DEFAULT)));
							
					}
					user.setImage(this.imageUser);
					this.model.getBundle().put(Bundle.USER, user);
					this.gestionnaire.modifierUser(this.model.getBundle());
				}
				else
				{
					this.model.getBundle().put(Bundle.MESSAGE, "Le pseudo ne peut pas être vide");
					this.model.getBundle().put(Bundle.OPERATION_REUSSIE, false);
				}
			}
			else
			{
				this.model.getBundle().put(Bundle.MESSAGE, "Mot de passe différent");
				this.model.getBundle().put(Bundle.OPERATION_REUSSIE, false);
			}
		}
		
		this.model.setBundle(this.model.getBundle());
		this.repaint();
	}

}
