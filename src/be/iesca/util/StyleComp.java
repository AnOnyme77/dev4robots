package be.iesca.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import be.iesca.controleur.GestionnaireUseCases;
import be.iesca.domaine.User;

/*
 * pour le style des diff��rents ��l��ments graphiques
 * Classe Cr��er par Hainaut mais style r��alis�� par Lammens
 */
@Author( auteur="Hainaut & Lammens")
@Version(version = 1)
public class StyleComp{
	public static Color BLANC = Color.WHITE;
	public static Color NOIR = Color.BLACK; 
	
	// choix du style a aporter
	public static final String JLABTITRE = "jlabTitre";
	public static final String JLABSOUSTITRE = "jlabSousTitre";
	public static final String JLABNOMUSER = "jlabNomUser";
	public static final String JBUTDECO = "jbutDeco";
	public static final String JBUTRETOUR = "jbRetour";
	public static final String JBUTCORP = "jbcorp";
	public static final String JTEXTFCORP = "jtextFCorp";
	public static final String JPASSFCORP = "jpassFCorp";
	public static final String JLABCORP = "jlabCorp";

	// valeur par default
	private static final String STYLEFONT = "Serif";
	
	private static String TITRE = "<HTML><u>Dev4Robot</u></HTML>";
	private static String SOUSTITRE = "<html><u>Sous Titre</u></html>";
	private static final String DECO = "Se déconnecter";
	private static final String RETOUR = "Retour";
	private static final String BUTONCORP = "Bouton";
	private static final String LABELCORP = "label";
	private static final String TEXTFCORP = "";
	
	private static final int SIZEFONTTITRE = 36;
	private static final int SIZEFONTBUTCORP = 20;
	private static final int SIZEFONTTEXTCORP = 16;
	private static final int SIZEFONTSOUSTITRE = 36;
	private static final int SIZEFONTNOMUSER = 30;
	private static final int SIZEFONTLABELCORP = 20;
	
	private static User USER = null;
	
	private StyleComp(){}

	@Author( auteur="Hainaut")
	@Version(version = 1)
	public static JComponent style(String typeComposant) {
		return style(typeComposant, null);
	}

	@Author( auteur="Hainaut")
	@Version(version = 1)
	public static JComponent style(String typeComposant, String text) {
		GestionnaireUseCases gest = GestionnaireUseCases.getInstance();
		USER = gest.getUser();
			
		switch(typeComposant){
			case JLABTITRE : return jlabTitre(text);
			case JLABSOUSTITRE : return jlabSousTitre( text);
			case JLABNOMUSER : return jlabNomUser( text);
			case JBUTDECO : return jbutDeco( text);
			case JBUTRETOUR : return jbutRetour( text);
			case JBUTCORP : return jbutCorp( text);
			case JTEXTFCORP : return jtextFCorp( text);
			case JPASSFCORP : return jpassFCorp();
			case JLABCORP : return jlabCorp(text);
			default : return null;
		}
	}


	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jlabTitre(String text){
		if(text == null) text=TITRE;
		JLabel composant = new JLabel(text);
		composant.setFont(new Font(STYLEFONT, Font.BOLD, SIZEFONTTITRE));
		composant.setOpaque(false);
		if(USER == null) composant.setForeground(NOIR);
		else composant.setForeground(BLANC);
		return composant;
	}

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jlabSousTitre(String text){
		if(text==null) text=SOUSTITRE;
		JLabel composant = new JLabel(text);
		composant.setPreferredSize(new Dimension(250,70));
		composant.setFont(new Font(STYLEFONT, Font.BOLD, SIZEFONTSOUSTITRE));
		composant.setHorizontalAlignment(SwingConstants.CENTER);
		composant.setForeground(NOIR);
		composant.setOpaque(false);
		return composant;
	}

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jlabNomUser(String text){
		if(USER != null){
			if(text == null) text = USER.getPseudo();
			JLabel composant = new JLabel(text);
			composant.setIcon( new ImageIcon(new ImageIcon(USER.getImage()).getImage().getScaledInstance(70, 70, Image.SCALE_DEFAULT)));
			composant.setForeground(BLANC);
			composant.setFont(new Font(STYLEFONT, Font.BOLD, SIZEFONTNOMUSER));
			composant.setHorizontalAlignment(SwingConstants.LEFT);
			composant.setOpaque(false);
			return composant;	
		}
		else return null;
		
	}

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jbutDeco(String text){
		if(USER != null){
			if(text == null) text = DECO;
			JButton composant = new JButton(text);
			composant.setPreferredSize(new Dimension(200,50));
			composant.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, NOIR));
			composant.setBackground(Color.LIGHT_GRAY);
			composant.setForeground(NOIR);
			composant.setFont(new Font(STYLEFONT, Font.BOLD, SIZEFONTBUTCORP));
			
			return composant;	
		}
		else return null;
	}

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jbutRetour(String text){
		if(text == null) text = RETOUR;
		JButton composant = new JButton(text);
		composant.setPreferredSize(new Dimension(200,50));
		composant.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, NOIR));
		composant.setBackground(Color.LIGHT_GRAY);
		composant.setForeground(NOIR);
		composant.setFont(new Font(STYLEFONT, Font.BOLD, SIZEFONTBUTCORP));
		
		return composant;
	}

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jbutCorp(String text){
		if(text == null) text = BUTONCORP;
		JButton composant = new JButton(text);
		composant.setPreferredSize(new Dimension(200,50));
		composant.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, NOIR));
		composant.setBackground(Color.LIGHT_GRAY);
		composant.setForeground(NOIR);
		composant.setFont(new Font(STYLEFONT, Font.BOLD, SIZEFONTBUTCORP));
		return composant;
	}

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jtextFCorp(String text){
		if(text == null) text = TEXTFCORP;
		JTextField composant = new JTextField(text);
		composant.setPreferredSize(new Dimension(200,50));
		composant.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, NOIR));
		composant.setBackground(BLANC);
		composant.setForeground(NOIR);
		composant.setFont(new Font(STYLEFONT, Font.PLAIN, SIZEFONTTEXTCORP));
		
		return composant;
	}
	
	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jpassFCorp(){
		JPasswordField composant = new JPasswordField();
		composant.setPreferredSize(new Dimension(200,50));
		composant.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, NOIR));
		composant.setBackground(BLANC);
		composant.setForeground(NOIR);
		composant.setFont(new Font(STYLEFONT, Font.PLAIN, SIZEFONTTEXTCORP));
		
		return composant;
	}
	

	@Author( auteur="Hainaut & Lammens")
	@Version(version = 1)
	private static JComponent jlabCorp(String text) {
		if(text == null) text = LABELCORP;
		JLabel composant = new JLabel(text);
		composant.setForeground(NOIR);
		composant.setFont(new Font(STYLEFONT, Font.PLAIN, SIZEFONTLABELCORP));
		
		return composant;
	}
}
