package be.iesca.util;

public @interface Author {
	String auteur();
	String [] reviseur() default {};
}
