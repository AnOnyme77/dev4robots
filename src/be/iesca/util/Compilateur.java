package be.iesca.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

import org.eclipse.jdt.core.compiler.CompilationProgress;
import org.eclipse.jdt.core.compiler.batch.BatchCompiler;

/**
 * Compilateur Java
 * 
 * Permet de compiler des classes Java dont on sp�cifie le r�pertoire source.
 * Les .class g�n�r�s sont ajout�s au r�pertoire de destination sp�cifi�. La
 * librairie "ecj.jar" doit avoir �t� ajout�e � l'application.
 * 
 * Pour plus d'infos :
 * http://help.eclipse.org/juno/index.jsp?topic=%2Forg.eclipse.jdt
 * .doc.user%2Ftasks%2Ftask-using_batch_compiler.htm
 * 
 */
public class Compilateur {
	private static final String CLASS_PATH = "-classpath rt.jar";
	private static final String VERSION_JAVA = "1.7"; // version par d�faut
	private static final String FICHIER_ERREURS = "./erreursCompilation.txt";
	private static final String FICHIER_OUTPUT = "./output.txt";
	private String paramLibrairies; // sp�cifie les librairies.
	// ex: -classpath rt.jar;./libs/robocode.jar
	private String repertoireSource; // chemin relatif ex: ./robots/robots
	private String versionJava; // ex: 1.7 pour Java 7
	private String repertoireDestination;
	private CompilationProgress listener;
	private String autreParam;
	private File fichierErreurs;
	private PrintStream output;
	
	/**
	 * Constructeur de la classe
	 * 
	 * @param repertoireSource
	 *            : r�pertoire contenant les fichiers � compiler. C'est un
	 *            chemin relatif. ex: ./robots/robots
	 * @param librairies
	 *            : table de String sp�cifiant les librairies � inclure au
	 *            CLASSPATH (ex:String[] librairies = { "./libs/robocode.jar"
	 *            };)
	 * @param repertoireDestination
	 *            : repertoire qui contiendra les fichiers .class (r�sultat de
	 *            la compilation). C'est un chemin relatif. ex: ./robots/robots
	 * @param sortieConsole
	 *            : si true : sortie � la console, si false : sortie dans un
	 *            fichier (FICHIER_OUTPUT)
	 */
	public Compilateur(String repertoireSource, String[] librairies,
			String repertoireDestination, boolean sortieConsole) {
		this(repertoireSource, librairies, repertoireDestination, VERSION_JAVA,
				null, null, sortieConsole);
	}

	/**
	 * Constructeur de la classe
	 * 
	 * @param repertoireSource
	 *            : r�pertoire contenant les fichiers � compiler. C'est un
	 *            chemin relatif. ex: ./robots/robots
	 * @param librairies
	 *            : table de String sp�cifiant les librairies � inclure au
	 *            CLASSPATH (ex:String[] librairies = { "./libs/robocode.jar"
	 *            };)
	 * @param repertoireDestination
	 *            : repertoire qui contiendra les fichiers .class (r�sultat de
	 *            la compilation). C'est un chemin relatif. ex: ./robots/robots.
	 * @param versionJava
	 *            : version du compilateur ex: "1.7" pour Java 7
	 * @param autreParam
	 *            : autre param�tre. ex: -time (pour afficher la dur�e de
	 *            compilation)
	 * @param listener
	 *            : listener � l'�coute du compilateur.
	 * @param sortieConsole
	 *            : si true : sortie � la console, si false : sortie dans un
	 *            fichier (FICHIER_OUTPUT)
	 */
	public Compilateur(String repertoireSource, String[] librairies,
			String repertoireDestination, String versionJava,
			String autreParam, CompilationProgress listener,
			boolean sortieConsole) {
		super();
		this.repertoireSource = repertoireSource;
		this.repertoireDestination = repertoireDestination;
		this.listener = listener;
		this.paramLibrairies = CLASS_PATH;
		for (String lib : librairies) {
			this.paramLibrairies += ";" + lib;
		}
		if (autreParam == null || autreParam.isEmpty())
			this.autreParam = "-time"; // affiche temps de compilation
		else
			this.autreParam = autreParam;
		if (versionJava == null || versionJava.isEmpty())
			this.versionJava = VERSION_JAVA;
		else
			this.versionJava = versionJava;
		this.fichierErreurs = new File(FICHIER_ERREURS);
		if (sortieConsole)
			this.output = System.out;
		else
			try {
				this.output = new PrintStream(new File(FICHIER_OUTPUT));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Lance le processus de compilation.
	 */
	public void compiler() {
		String parametres = repertoireSource + " -" + versionJava + " "
				+ paramLibrairies + " -d " + repertoireDestination + " "
				+ autreParam;
		this.output.println(parametres);
		try {
			BatchCompiler.compile(parametres, new PrintWriter(this.output),
					new PrintWriter(this.fichierErreurs), listener);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (!compilationReussie()) {
			this.output.println("Il y a des erreurs de compilation.");
			this.output.println("Consultez le fichier : " + FICHIER_ERREURS);
		}
	}

	/**
	 * Permet de savoir si la compilation a r�ussi.
	 * 
	 * @return un bool�en signalant si la compilation est un succ�s ou pas.
	 */
	public boolean compilationReussie() {
		boolean compilationReussie = false;
		FileReader reader = null;
		try {
			reader = new FileReader(fichierErreurs);
			if (reader.read() == -1)
				compilationReussie = true;
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (reader != null)
					reader.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return compilationReussie;
	}
}
