package be.iesca.util;

public @interface Version {
	
	int version() default 1;
}


