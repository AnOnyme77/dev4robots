package be.iesca.dao;

import java.util.ArrayList;

import be.iesca.domaine.Bataille;

public interface BatailleDao extends Dao{
	public boolean ajouterBataille(Bataille bataille);
	public boolean modifierBataille(Bataille bataille);
	public boolean supprimerBataille(Bataille bataille);
	public ArrayList<Bataille> listerBataille(Bataille bataille);
	public Bataille recupererBataille(Bataille bataille);
}
