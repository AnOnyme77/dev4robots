package be.iesca.dao;


import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author( auteur="Lammens")
@Version(version = 1)
public interface UserDao extends Dao {
	User getUser(String email, String password);
	User getUser(String email);
	boolean ajouterUser(User user);
	boolean modifierUser(User user);
	
}
