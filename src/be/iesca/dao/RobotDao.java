package be.iesca.dao;

import java.util.ArrayList;

import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;


@Author( auteur="Evrard", reviseur={"Jamar"})
@Version(version = 1)
public interface RobotDao extends Dao{
	ClasseRobot getRobot(ClasseRobot robot, User utilisateur);
	boolean ajouterRobot(ClasseRobot robot, User utilisateur);
	boolean supprimerRobot(ClasseRobot currentRobot,User utilisateur);
	ArrayList <ClasseRobot> listerRobots();
	ArrayList <ClasseRobot> listerRobotsUser(User user);
	boolean robotUserExist(ClasseRobot robot,User utilisateur);
	boolean robotExist(ClasseRobot robot);
	public boolean ajouterUserRobot(ClasseRobot robot, User utilisateur);
	public boolean supprimerUserRobot(ClasseRobot currentRobot, User utilisateur);
	public boolean robotIsUsed(ClasseRobot currentRobot);
}
