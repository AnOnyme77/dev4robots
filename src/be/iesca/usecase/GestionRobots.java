package be.iesca.usecase;

import be.iesca.domaine.Bundle;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author(auteur="Evrard")
@Version(version=1)
public interface GestionRobots 
{
	void ajouterRobot(Bundle bundle);
	void supprimerRobot(Bundle bundle);
	void viderRobots(Bundle bundle);
	void listerRobots(Bundle bundle);
	void listerRobotsUser(Bundle bundle);

}
