package be.iesca.usecase;

import be.iesca.domaine.Bundle;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author( auteur="Legrand", reviseur={"Hainaut"})
@Version(version = 1)
public interface GestionUsers {
	void connecterUser(Bundle bundle); // correspond à rechercherUser
	void ajouterUser(Bundle bundle);
	void modifierUser(Bundle bundle);
}