package be.iesca.usecase;

import be.iesca.domaine.Bundle;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author(auteur="Jamar")
@Version(version=1)
public interface GestionBatailles 
{
	void ajouterBataille(Bundle bundle);
	void listerBataille(Bundle bundle);
	void modifierBataille(Bundle bundle);
	void lancerBataille(Bundle bundle);
	void supprimerBataille(Bundle bundle);
	void recupererBataille(Bundle bundle);
}
