package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import be.iesca.dao.RobotDao;
import be.iesca.domaine.Bundle;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Fichier;
import be.iesca.util.Version;

@Author( auteur="Evrard & Jamar")
public class RobotDaoImpl implements RobotDao {
	private static final String LISTER = "select * from robots";
	private static final String LISTERUSER = "select * from userrobots where email=?";
	private static final String AJOUTERROBOT = "insert into robots(nom,codesource) values(?,?)";
	private static final String AJOUTERUSERROBOT = "insert into userrobots(email,nom) values(?,?)";
	private static final String GETROBOT = "select * from robots where nom=?";
	private static final String GETUSERROBOT = "select * from userrobots where nom=? and email=?";
	private static final String SUPPRIMERROBOT = "delete from robots where nom=?";
	private static final String SUPPRIMERUSERROBOT = "delete from userrobots where nom=? and email=?";
	private static final String ROBOTISUSED = "select count(*) from userrobots where nom=?";
	private static final String EXT = ".java";
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Override
	public ArrayList<ClasseRobot> listerRobots() {
		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//Variables d'utilisation d'objets
		ArrayList<ClasseRobot> liste = new ArrayList<ClasseRobot>();
		ClasseRobot returnedRobot = null;
		
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTER);
			//Aucuns parametres requis pour la QUERY
			rs = ps.executeQuery();
			while (rs.next()) {
				//Récupération du robot courrant dans la liste
				returnedRobot = new ClasseRobot();
				returnedRobot.setNom(rs.getString("nom"));
				liste.add(returnedRobot);	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return liste;
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	@Override
	public ArrayList<ClasseRobot> listerRobotsUser(User user) {
		ArrayList<ClasseRobot> resultat = new ArrayList<ClasseRobot>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ClasseRobot robot;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTERUSER);
			ps.setString(1, user.getEmail().trim());
			rs=ps.executeQuery();
			while(rs.next()){
				robot = new ClasseRobot();
				robot.setNom(rs.getString("nom"));
				resultat.add(robot);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return resultat;
	}

	@Author( auteur="Jamar")
	@Version(version = 1)
	public ClasseRobot getRobot(ClasseRobot robot, User utilisateur) {

		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//Variables d'utilisation d'objets
		ClasseRobot returnedRobot = null;
		
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GETROBOT);
			//Passages des parametres à la QUERY
			ps.setString(1, robot.getNom().trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				//Récupération du résultat
				returnedRobot = new ClasseRobot();
				returnedRobot.setNom(rs.getString("nom"));
				Fichier.creerFichier(rs, "codesource", Bundle.REPERTOIRE_ROBOTS, returnedRobot.getNom()+EXT);	
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return returnedRobot;
	}
	
	@Author( auteur="Evrard")
	@Version(version = 1)
	public boolean robotUserExist(ClasseRobot robot,User utilisateur) {
		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//Variables d'utilisation d'objets
		boolean isExist = false;
		
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GETUSERROBOT);
			//Passages des parametres à la QUERY
			ps.setString(1, robot.getNom().trim());
			ps.setString(2, utilisateur.getEmail().trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				//Si le robot existe du résultat
				isExist = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return isExist;
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	public boolean robotExist(ClasseRobot robot) {
		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//Variables d'utilisation d'objets
		boolean isExist = false;
		
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GETROBOT);
			//Passages des parametres à la QUERY
			ps.setString(1, robot.getNom().trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				//Si le robot existe ..
				isExist = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return isExist;
	}

	@Author( auteur="Evrard")
	@Version(version = 1)
	@Override
	public boolean ajouterRobot(ClasseRobot robot, User utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean ajoutReussi=false;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUTERROBOT);
			ps.setString(1, robot.getNom());
			
			Fichier.lectureEtprepareStatement(Bundle.REPERTOIRE_ROBOTS, robot.getNom()+EXT, ps, 2);
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajoutReussi = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return ajoutReussi;
	}

	@Author( auteur="Evrard")
	@Version(version = 1)
	public boolean ajouterUserRobot(ClasseRobot robot, User utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean ajoutReussi=false;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUTERUSERROBOT);
			ps.setString(1, utilisateur.getEmail());
			ps.setString(2, robot.getNom());
			
			int resultat = ps.executeUpdate();
			if (resultat == 1) {
				ajoutReussi = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return ajoutReussi;
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	@Override
	public boolean supprimerRobot(ClasseRobot currentRobot, User utilisateur) {
		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nbSupp = 0;
		//Variables d'utilisation d'objets
		boolean operation = false;
		
		//Suppression au niveau de la table : userrobots
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPPRIMERROBOT);
			//Passages des parametres à la QUERY
			ps.setString(1, currentRobot.getNom().trim());
			nbSupp = ps.executeUpdate();
			if (nbSupp == 1) {
				//Si la QUERY me renvoie 1 alors l'opération a réussie, on peut supprimer le fichier local.
				try{
					Fichier.supprimerUnFichier(currentRobot.getNom()+EXT, Bundle.REPERTOIRE_ROBOTS);
					operation = true;
				}catch(Exception ex){
					ex.printStackTrace();
					operation = false;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return operation;
	}
	
	@Author( auteur="Jamar")
	@Version(version = 1)
	public boolean supprimerUserRobot(ClasseRobot currentRobot, User utilisateur) {
		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nbSupp = 0;
		//Variables d'utilisation d'objets
		boolean operation = false;
		
		//Suppression au niveau de la table : userrobots
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPPRIMERUSERROBOT);
			//Passages des parametres à la QUERY
			ps.setString(1, currentRobot.getNom().trim());
			ps.setString(2, utilisateur.getEmail().trim());
			nbSupp = ps.executeUpdate();
			if (nbSupp == 1) {
				//Si la QUERY me renvoie 1 alors l'opération a réussie.
				operation = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return operation;
	}
	
	@Override
	public boolean robotIsUsed(ClasseRobot currentRobot) {
		//Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//Variables d'utilisation d'objets
		boolean isUsed = false;
		
		//Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(ROBOTISUSED);
			//Passages des parametres à la QUERY
			ps.setString(1, currentRobot.getNom().trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				//Si le robot est utilisé ..
				int count = rs.getInt(1);
				if(count > 0)
					isUsed = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return isUsed;
	}
	
	@Author( auteur="Legrand")
	@Version(version = 1)
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}

}
