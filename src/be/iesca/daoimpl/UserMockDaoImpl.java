package be.iesca.daoimpl;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import be.iesca.dao.UserDao;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;


public class UserMockDaoImpl implements UserDao {
	
	private Map<String, User> mapUser;
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	private class ComparateurUser implements Comparator<String> 
	{
		@Override
		public int compare(String nom1, String nom2) 
		{
			return nom1.compareTo(nom2);
		}
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	public UserMockDaoImpl() {
		super();
		Comparator<String> comp = new ComparateurUser();
		this.mapUser = new TreeMap<String, User>(comp);	
	}

	@Author( auteur="Lammens", reviseur={"Haianut"})
	@Version(version = 2)
	@Override
	public User getUser(String email, String password) {
		
		User user = this.mapUser.get(email);
		if(user != null && password !=null){
			if(!user.getPassword().equals(password)){// equals pas == ou !=
				user=null;
				
			}
		}
		return user;
	}
	
	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public User getUser(String email){
		return this.getUser(email, null);
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public boolean ajouterUser(User user) 
	{
		
		// je ne verifie pas si l'utilisateur existe deja
		User userDB= new User(user);
		this.mapUser.put(userDB.getEmail(), userDB);
		
		return (this.mapUser.containsKey(userDB.getEmail()));
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public boolean modifierUser(User user) {
		
		if(this.mapUser.get(user.getEmail())==null)
			return false;
		else
		{
			User userDB= new User(user);
			this.mapUser.put(userDB.getEmail(),  userDB);
			return (this.mapUser.containsKey(userDB.getEmail()));
		}
	}

}
