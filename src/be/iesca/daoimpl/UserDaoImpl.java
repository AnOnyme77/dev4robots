package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import be.iesca.dao.UserDao;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;


public class UserDaoImpl implements UserDao {
	private static final String GET = "SELECT * FROM users WHERE email=? and password = ?;";
	private static final String ADD = "insert into users ( email, password, pseudo, imageprofil) VALUES (?,?,?,?);";
	private static final String UPDATE = "UPDATE users SET pseudo = ?, imageprofil = ?, password = ? WHERE email = ?;";
	private static final String EXIST = "SELECT email FROM users WHERE email=?;";

	// obligatoire pour pouvoir construire une instance avec newInstance() 
	public UserDaoImpl() {
	}

	@Author( auteur="Legrand", reviseur={"Lammens"})
	@Version(version = 1)
	@Override
	public User getUser(String email, String password) {
		User user = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GET);
			ps.setString(1, email.trim());
			ps.setString(2, password.trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setPseudo(rs.getString("pseudo"));
				user.setEmail(rs.getString("email"));
				user.setPassword(rs.getString("password"));
				user.setImage( rs.getString("imageprofil"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return user;
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public boolean ajouterUser(User user) {

		boolean retour=false;
		
		if(getUser(user.getEmail()) == null)
		{
			Connection con = null;
			PreparedStatement ps = null;
			try {
				con = DaoFactory.getInstance().getConnexion();
				ps = con.prepareStatement(ADD);
				ps.setString(3, user.getPseudo().trim());
				ps.setObject(4, user.getImage());
				ps.setString(2, user.getPassword().trim());
				ps.setString(1, user.getEmail().trim());
				int resultat = ps.executeUpdate();
				if (resultat == 1) 
					retour = true;
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				cloturer(null, ps, con);
			}
		}
		return retour;
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public boolean modifierUser(User user) {
		
		boolean retour=false;
		
		if(getUser(user.getEmail()) != null)
		{
			Connection con = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				con = DaoFactory.getInstance().getConnexion();
				ps = con.prepareStatement(UPDATE);
				ps.setString(1, user.getPseudo().trim());
				ps.setObject(2, user.getImage());
				ps.setString(3, user.getPassword().trim());
				ps.setString(4, user.getEmail().trim());

				int resultat = ps.executeUpdate();
				if (resultat == 1) 
					retour = true;
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				cloturer(rs, ps, con);
			}
		}
		
		return retour;
	}

	@Author( auteur="Lammens")
	@Version(version = 1)
	@Override
	public User getUser(String email) {
		User user = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(EXIST);
			ps.setString(1, email.trim());
			rs = ps.executeQuery();
			if (rs.next()) {
				user = new User();
				user.setEmail(rs.getString("email"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		
		return user;
	}

	@Author( auteur="Legrand", reviseur={"Lammens"})
	@Version(version = 1)
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}
	
}