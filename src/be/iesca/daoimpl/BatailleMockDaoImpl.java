package be.iesca.daoimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import be.iesca.dao.BatailleDao;
import be.iesca.domaine.Bataille;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author(auteur="tous")
@Version(version = 1)
public class BatailleMockDaoImpl implements BatailleDao{

	/* /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\ 
	 * /!\  La clé dans la map doit être constituée de nomBataille + emailUser /!\
	 *  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\  /!\ 
	*/
	
	private Map<String,Bataille> listeBataille;
	
	public BatailleMockDaoImpl(){
		this.listeBataille=new HashMap<String,Bataille>();
	}
	
	@Author(auteur="Jamar")
	@Version(version = 1)
	@Override
	public boolean ajouterBataille(Bataille bataille) {
		boolean ajoutReussi=false;
		String cle;
		cle=bataille.getNom()+bataille.getEmail();
		if(!this.listeBataille.containsKey(cle)){
			this.listeBataille.put(cle, bataille);
			if(this.listeBataille.containsKey(cle))
				ajoutReussi=true;
		}
		return ajoutReussi;
	}

	@Author(auteur="Lammens")
	@Version(version = 1)
	@Override
	public boolean modifierBataille(Bataille bataille) {
		boolean retour=false;
		String cle;
		
		cle=bataille.getNom()+bataille.getEmail();
		
		if(this.listeBataille.containsKey(cle))
		{
			this.listeBataille.put(cle, bataille);
			if(this.listeBataille.containsKey(cle))
				retour=true;
		}
		return retour;
	}

	@Author(auteur="Hainaut")
	@Version(version = 1)
	@Override
	public boolean supprimerBataille(Bataille bataille) {
		try {
			if (this.listeBataille.remove(bataille.getNom()+bataille.getEmail()) == null)
				return false;
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@Author(auteur="Evrard")
	@Version(version = 1)
	@Override
	public ArrayList<Bataille> listerBataille(Bataille bataille) {
		Collection<Bataille> myListe = Collections.unmodifiableCollection(this.listeBataille.values());
		return new ArrayList<Bataille>(myListe);
	}

	@Author(auteur="Evrard")
	@Version(version = 1)
	@Override
	public Bataille recupererBataille(Bataille bataille) {
		Bataille retour=null;
		if(this.listeBataille.containsKey(bataille.getNom()+bataille.getEmail()))
			retour=this.listeBataille.get(bataille.getNom()+bataille.getEmail());
		return retour;
	}

}
