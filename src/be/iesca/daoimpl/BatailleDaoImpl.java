package be.iesca.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import be.iesca.dao.BatailleDao;
import be.iesca.dao.RobotDao;
import be.iesca.domaine.Bataille;
import be.iesca.domaine.ClasseRobot;
import be.iesca.util.Author;
import be.iesca.util.Version;

public class BatailleDaoImpl implements BatailleDao {

	private static final String AJOUTERBATAILLE = "insert into batailles(nom,email) values(?,?)";
	private static final String AJOUTERCOMBATTANT = "insert into combattants(nomrobot,email,nombataille) values(?,?,?)";

	private static final String SUPBATAILLE = "delete from batailles where nom=? and email=?";
	private static final String SUPCOMBATTANTS = "delete from combattants where nombataille=? and email=?";
	
	private static final String GETCOMBATTANTBATAILLE = "select * from combattants where nombataille=? and email=?";
	private static final String LISTERBATAILLE = "select * from batailles where email=?";
	private static final String DELCOMBATTANT = "delete from combattants where nombataille=? and nomrobot=? and email=?";
	private RobotDao robotDao = new RobotDaoImpl();

	@Author(auteur = "Jamar")
	@Version(version = 1)
	@Override
	public boolean ajouterBataille(Bataille bataille) {
		ClasseRobot robot = new ClasseRobot();
		Connection con = null;
		PreparedStatement ps = null;
		boolean ajoutReussiPrec = false;
		String[] listeRobots = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(AJOUTERBATAILLE);
			ps.setString(1, bataille.getNom());
			ps.setString(2, bataille.getEmail());
			int resultat = ps.executeUpdate();
			if (resultat == 1)
				ajoutReussiPrec = true;
			if (ajoutReussiPrec) {
				// Si l'ajout dans la table bataille a réussi, on ajoute dans la
				// table combattants
				listeRobots = bataille.listeRobots();
				// Pour chaque robot dans la liste de la bataille on va vérifier
				// si le robot existe
				for (String nameRobot : listeRobots) {
					robot.setNom(nameRobot);
					if (this.robotDao.robotExist(robot) && ajoutReussiPrec) {
						/*
						 * Ici pour le 1er robot, ajoutReussiPrec dépendra de
						 * l'ajout de la bataille dans la table batailles, après
						 * il prendra la valeur d'ajout du robot précédent.
						 */
						try {
							// S'il existe, on l'ajoute dans la table combattant
							ps = con.prepareStatement(AJOUTERCOMBATTANT);
							ps.setString(1, robot.getNom());
							ps.setString(2, bataille.getEmail());
							ps.setString(3, bataille.getNom());
							resultat = ps.executeUpdate();
							if (resultat == 1)
								ajoutReussiPrec = true;
							else
								ajoutReussiPrec = false;
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(null, ps, con);
		}
		return ajoutReussiPrec;
	}

	@Author(auteur = "Lammens")
	@Version(version = 1)
	@Override
	public boolean modifierBataille(Bataille bataille) {
		ClasseRobot robot = new ClasseRobot();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean modifReussi=false, robotExist=true;
		
		String[] listeRobot = bataille.listeRobots();
		ArrayList<String> listeRobotDB = new ArrayList<String>();
		
		// on verifie si tout les robot existe
		for (String nameRobot : listeRobot) 
		{
			robot.setNom(nameRobot);
			if (!this.robotDao.robotExist(robot))
				robotExist = false;
		}
		
		// si un robot est inexistant, on ne fait aucune modification
		if(robotExist)
		{
			try 
			{
				// recuperation du tableau des combattants de la bataille
				con = DaoFactory.getInstance().getConnexion();
				ps = con.prepareStatement(GETCOMBATTANTBATAILLE);
				ps.setString(1, bataille.getNom());
				ps.setString(2, bataille.getEmail());
				rs = ps.executeQuery();
				
				while(rs.next()) 
				{
					listeRobotDB.add(rs.getString("nomrobot"));
				}
				
				// ajout des combatant non present en DB
				for(int i=0; i<listeRobot.length; i++)
				{
					if(!listeRobotDB.contains(listeRobot[i]))
					{
						listeRobotDB.add(listeRobot[i]);
						ps = con.prepareStatement(AJOUTERCOMBATTANT);
						ps.setString(3, bataille.getNom());
						ps.setString(2, bataille.getEmail());
						ps.setString(1, listeRobot[i]);
						ps.executeUpdate();
					}
				}
				
				// suppression des combatant non present dans la bataille mais present en DB
				if(listeRobotDB.size()!=listeRobot.length)
				{
					for(int i = 0; i<listeRobot.length; i++)
					{
						if(listeRobotDB.contains(listeRobot[i]))
							listeRobotDB.remove(listeRobot[i]);
					}
					for (String Robot : listeRobotDB) 
					{
						ps = con.prepareStatement(DELCOMBATTANT);
						ps.setString(1, bataille.getNom());
						ps.setString(2, Robot);
						ps.setString(3, bataille.getEmail());
						ps.executeUpdate();
					}
				}
							
				modifReussi=true;
				
			} 
			catch (Exception ex) 
			{
				ex.printStackTrace();
			} finally {
				cloturer(null, ps, con);
			}
		}

		return modifReussi;
	}

	@Author(auteur = "Hainaut")
	@Version
	@Override
	public boolean supprimerBataille(Bataille bataille) {
		boolean suppressionReussie = false;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPCOMBATTANTS);
			ps.setString(1, bataille.getNom().trim());
			ps.setString(2, bataille.getEmail().trim());
			int nb = ps.executeUpdate();
			if (nb < 1){
				return false;
			}

			// cloturer avant de relancer une seconde requette
			cloturer(rs, ps, con); 
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(SUPBATAILLE);
			ps.setString(1, bataille.getNom().trim());
			ps.setString(2, bataille.getEmail().trim());
			nb = ps.executeUpdate();
			if (nb == 1) {
				suppressionReussie = true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return suppressionReussie;
	}

	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Override
	public ArrayList<Bataille> listerBataille(Bataille bataille) {
		ResultSet rs = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rsCombattants = null;
		PreparedStatement psCombattants = null;

		ArrayList<Bataille> listeBataille = new ArrayList<Bataille>();
		Bataille batailleActuelle;
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(LISTERBATAILLE);
			ps.setString(1, bataille.getEmail());
			rs = ps.executeQuery();
			while (rs.next()) {
				batailleActuelle = new Bataille();
				batailleActuelle.setNom(rs.getString("nom"));
				batailleActuelle.setEmail(bataille.getEmail());
				try {
					psCombattants = con.prepareStatement(GETCOMBATTANTBATAILLE);
					psCombattants.setString(1, bataille.getNom());
					psCombattants.setString(2, bataille.getEmail());
					rsCombattants = psCombattants.executeQuery();
					while (rsCombattants.next()) {
						batailleActuelle.ajouterRobot(rsCombattants.getString("nomrobot"));
					}
					listeBataille.add(batailleActuelle);
				} catch (Exception ex) {
					ex.printStackTrace();
				} finally {
					cloturer(rsCombattants, null, null);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}
		return listeBataille;
	}

	@Author(auteur = "Evrard")
	@Version(version = 1)
	@Override
	public Bataille recupererBataille(Bataille bataille) {
		// Déclaration des variables de connexion
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Bataille retour = null;
		// Début de la connexion à la DB
		try {
			con = DaoFactory.getInstance().getConnexion();
			ps = con.prepareStatement(GETCOMBATTANTBATAILLE);

			// Passages des parametres à la QUERY
			ps.setString(1, bataille.getNom());
			ps.setString(2, bataille.getEmail());
			rs = ps.executeQuery();
			if(rs.next()){
				retour=new Bataille();
				retour.setEmail(bataille.getEmail());
				retour.setNom(bataille.getNom());
	
				do{
					retour.ajouterRobot(rs.getString("nomrobot"));
				}while(rs.next());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			cloturer(rs, ps, con);
		}

		return retour;
	}

	@Author(auteur = "Legrand")
	@Version(version = 1)
	private void cloturer(ResultSet rs, PreparedStatement ps, Connection con) {
		try {
			if (rs != null)
				rs.close();
		} catch (Exception ex) {
		}
		try {
			if (ps != null)
				ps.close();
		} catch (Exception ex) {
		}
		try {
			if (con != null)
				con.close();
		} catch (Exception ex) {
		}
	}

}
