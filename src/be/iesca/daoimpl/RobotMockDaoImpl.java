package be.iesca.daoimpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import be.iesca.dao.RobotDao;
import be.iesca.domaine.ClasseRobot;
import be.iesca.domaine.User;
import be.iesca.util.Author;
import be.iesca.util.Version;

@Author( auteur="Evrard & Jamar")
public class RobotMockDaoImpl implements RobotDao {
	
	private Map<String, ClasseRobot> mapRobot;
	
	public RobotMockDaoImpl(){
		this.mapRobot= new TreeMap<String, ClasseRobot>();
	}
	
	@Author(auteur="Evrard")
	@Version(version=2)
	@Override
	public ClasseRobot getRobot(ClasseRobot robot,User utilisateur) {
		ClasseRobot retour=null;
		if(this.mapRobot.containsKey(robot.getNom()+utilisateur.getEmail()))
			retour=this.mapRobot.get(robot.getNom()+utilisateur.getEmail());
		return retour;
	}
	
	@Author(auteur="Evrard")
	@Version(version=2)
	@Override
	public boolean ajouterRobot(ClasseRobot robot,User utilisateur) {
		boolean retour=false;
		String cle;
		cle=robot.getNom()+utilisateur.getEmail();
		if(!this.mapRobot.containsKey(cle)){
			this.mapRobot.put(cle, robot);
			if(this.mapRobot.containsKey(cle))
				retour=true;
		}
		return retour;
	}

	@Author(auteur="Jamar",reviseur={"Evrard"})
	@Version(version=2)
	@Override
	public boolean supprimerRobot(ClasseRobot currentRobot,User utilisateur) 
	{
		boolean retour = true;
		String idRobot = currentRobot.getNom()+utilisateur.getEmail();
		
		if(this.mapRobot.containsKey(idRobot))
			//Si le robot existe dans la map, on le supprime
			this.mapRobot.remove(idRobot);
		else
			//Si pas, on ne fait rien sauf retourner false pour un echec
			retour = false;
		
		return retour;
	}

	@Override
	public ArrayList <ClasseRobot> listerRobots() 
	{
		//Listage des robots avec une collection.
		Collection<ClasseRobot> myListe = Collections.unmodifiableCollection(this.mapRobot.values());
		return new ArrayList<ClasseRobot>(myListe);
	}
	
	@Author(auteur="Evrard")
	@Version(version=1)
	@Override
	public ArrayList <ClasseRobot> listerRobotsUser(User user) 
	{
		//Listage des robots avec une collection.
		ArrayList<ClasseRobot> liste =  new ArrayList<ClasseRobot>();
		for(String idRobot: this.mapRobot.keySet()){
			if(idRobot.contains(user.getEmail()) && !liste.contains(this.mapRobot.get(idRobot)))
				liste.add(this.mapRobot.get(idRobot));
		}
		return liste;
	}

	@Override
	public boolean robotUserExist(ClasseRobot robot, User utilisateur) {
		//inutile ici
		return false;
	}

	@Override
	public boolean robotExist(ClasseRobot robot) {
		// Inutile ici
		return false;
	}

	@Override
	public boolean ajouterUserRobot(ClasseRobot robot, User utilisateur) {
		//inutile ici
		return false;
	}

	@Override
	public boolean supprimerUserRobot(ClasseRobot currentRobot, User utilisateur) {
		//inutile ici
		return false;
	}

	@Override
	public boolean robotIsUsed(ClasseRobot currentRobot) {
		// inutile ici
		return false;
	}

}
